use CustomerManagementSystemDB

INSERT INTO sim_price(sp_sim_price) VALUES(150.00)

select * From mobile_numbers

INSERT INTO customer(customer_name,customer_nic,customer_email,customer_dob,userName,userPassword,customer_joinDate) VALUES('USER 1','xxxxx-xxxxxxx-x','user1@gmail.com','20120618 10:34:09 AM','user1','123','20150318 10:30:00 AM')
INSERT INTO customer(customer_name,customer_nic,customer_email,customer_dob,userName,userPassword,customer_joinDate) VALUES('USER 2','xxxxx-xxxxxxx-x','user2@gmail.com','20020117 10:34:09 AM','user2','123','20150109 08:30:00 PM')

INSERT INTO mobile_numbers(mn_number) VALUES('03333902985')
INSERT INTO mobile_numbers(mn_number) VALUES('03333902986')

INSERT INTO registered_number(customerID,mobile_number) VALUES(1,'03333902985')
UPDATE mobile_numbers SET mn_available=0 WHERE mn_number='03333902985' 
INSERT INTO registered_number(customerID,mobile_number) VALUES(2,'03333902986')
UPDATE mobile_numbers SET mn_available=0 WHERE mn_number='03333902986' 
INSERT INTO sim_sale_invoice(ssi_mobileNumber,ssi_cid,ssi_sim_price,ssi_DateTime) VALUES('03333902985',1,1,'20120618 10:34:09 AM')
INSERT INTO sim_sale_invoice(ssi_mobileNumber,ssi_cid,ssi_sim_price,ssi_DateTime) VALUES('03333902986',2,1,'20110208 09:34:09 PM')

INSERT INTO call_unit_price(cup_unit_charges,cup_DateTime) VALUES(3.75,'20010101 11:00:00 AM')
INSERT INTO call_unit_price(cup_unit_charges,cup_DateTime) VALUES(2.15,'20020101 10:00:00 PM')

INSERT INTO call_record(call_from,call_to,call_duration,call_unit_charges) VALUES(1,2,2.54,1)
INSERT INTO call_record(call_from,call_to,call_duration,call_unit_charges) VALUES(2,1,1.15,2)

INSERT INTO package(p_title,p_desc) VALUES('Package 1','Package 1 Desc')
INSERT INTO package(p_title,p_desc) VALUES('Package 2','Package 2 Desc')

INSERT INTO discount_offers(do_title,do_desc,do_packageID,do_percentage_off) VALUES('Discount Offer 1','Discount Offer 1 Desc',1,5.00)
INSERT INTO discount_offers(do_title,do_desc,do_packageID,do_percentage_off) VALUES('Discount Offer 2','Discount Offer 2 Desc',2,7.00)

INSERT INTO customer_package(cp_cid,cp_pid) VALUES(1,2)
INSERT INTO customer_package(cp_cid,cp_pid) VALUES(2,1)

INSERT INTO department(d_name) VALUES('Department 1')
INSERT INTO department(d_name) VALUES('Department 2')

INSERT INTO memo(m_title,m_desc,m_department) VALUES('Memo 1','Memo 1 Desc',1)
INSERT INTO memo(m_title,m_desc,m_department) VALUES('Memo 2','Memo 2 Desc',2)

INSERT INTO budget(b_month,b_value) VALUES('20010101 11:00:00 AM',45000)
INSERT INTO budget(b_month,b_value) VALUES('20100101 12:00:00 AM',55000)

INSERT INTO marketing_campaign(mc_title,mc_desc,mc_approval_status,mc_budget,mc_department,mc_DATETIME) VALUES('Marketing Campaign 1','Marketing Campaign 1 Desc',0,10000,1,'20070504 11:00:00 AM')
INSERT INTO marketing_campaign(mc_title,mc_desc,mc_approval_status,mc_budget,mc_department,mc_DATETIME) VALUES('Marketing Campaign 2','Marketing Campaign 2 Desc',0,5000,2,'20080808 08:08:00 AM')

INSERT INTO employee(e_name,e_email,e_contact,userName,userPassword,userRole,e_dID) VALUES('Employee1','employee1@gmail.com','0333333333','employee1','123',2,1)
INSERT INTO employee(e_name,e_email,e_contact,userName,userPassword,userRole,e_dID) VALUES('Employee2','employee2@gmail.com','0333333333','employee2','123',2,2)

INSERT INTO customer_complaint(cc_title,cc_desc,cc_department,cc_DATETIME,cc_cid) VALUES('Complaint 1','Complaint 1 Desc',1,'20010101 11:00:00 AM',1)
INSERT INTO customer_complaint(cc_title,cc_desc,cc_department,cc_DATETIME,cc_cid) VALUES('Complaint 2','Complaint 2 Desc',1,'20010101 01:00:00 PM',2)

INSERT INTO customer_feedback(cfb_title,cfb_desc,cfb_DATETIME,cfb_cid) VALUES('Customer Feedback 1','Customer Feedback 1 Desc','20080107 11:00:00 AM',2)
INSERT INTO customer_feedback(cfb_title,cfb_desc,cfb_DATETIME,cfb_cid) VALUES('Customer Feedback 2','Customer Feedback 2 Desc','20090107 11:00:00 AM',1)