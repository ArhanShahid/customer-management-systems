--**********************
-- Delete DataBase
--**********************

--DROP DATABASE CustomerManagementSystemDB

--**********************
-- CREATE DataBase
--**********************

CREATE DATABASE CustomerManagementSystemDB

--**********************
-- Use DataBase
--**********************

use CustomerManagementSystemDB
--**********************
-- CREATE Tables
--**********************

CREATE TABLE sim_price(
sp_id INT IDENTITY(1,1) PRIMARY KEY,
sp_DATETIME DATETIME DEFAULT GETDATE(),
sp_sim_price DECIMAL(18,4) Not Null,
);

CREATE TABLE customer(
customer_id INT IDENTITY(1,1) PRIMARY KEY,
customer_name VARCHAR(255) NOT NULL,
customer_nic VARCHAR(255) NOT NULL,
customer_email VARCHAR(255) NOT NULL,
customer_dob DATETIME DEFAULT null,
userName VARCHAR(50) NOT NULL,
userPassword VARCHAR(50) NOT NULL,
userRole INT DEFAULT 1,
customer_joinDate DATETIME DEFAULT GETDATE()
);

CREATE TABLE mobile_numbers(
mn_number VARCHAR(50) NOT NULL PRIMARY KEY,
mn_available Bit DEFAULT 1,
);

CREATE TABLE sim_sale_invoice(
ssi_id INT IDENTITY(1,1) PRIMARY KEY,
ssi_mobileNumber VARCHAR(50) NOT NULL,
ssi_cid INT NOT NULL,
ssi_sim_price INT NOT NULL,
ssi_DateTime DATETIME DEFAULT GETDATE(),

CONSTRAINT fk_invoice_customer FOREIGN KEY (ssi_cid)
REFERENCES customer(customer_id),

CONSTRAINT fk_sim_price FOREIGN KEY (ssi_sim_price)
REFERENCES sim_price(sp_id)
);

CREATE TABLE registered_number
(
mobile_number VARCHAR(50) NOT NULL,
customerID INT NOT NULL,

CONSTRAINT fk_registered_customer FOREIGN KEY (customerID)
REFERENCES customer(customer_id),

CONSTRAINT fk_registered_number FOREIGN KEY (mobile_number)
REFERENCES mobile_numbers(mn_number)
);

CREATE TABLE call_unit_price(
cup_id INT IDENTITY(1,1) PRIMARY KEY,
cup_DateTime DATETIME DEFAULT GETDATE(),
cup_unit_charges DECIMAL(18,4) Not Null,
)

CREATE TABLE call_record(
call_id INT IDENTITY(1,1) PRIMARY KEY,
call_from INT NOT NULL,
call_to INT Not Null,
call_duration DECIMAL(18,4) NOT NULL,
call_unit_charges INT Not Null

CONSTRAINT fk_customer_from FOREIGN KEY (call_from)
REFERENCES customer(customer_id),

CONSTRAINT fk_customer_to FOREIGN KEY (call_to)
REFERENCES customer(customer_id),

CONSTRAINT fk_call_record_call_unit_price FOREIGN KEY (call_unit_charges)
REFERENCES call_unit_price(cup_id),
);

CREATE TABLE package(
p_id INT IDENTITY(1,1) PRIMARY KEY,
p_title VARCHAR(50) NOT NULL,
p_desc VARCHAR(255) NOT NULL,
);

CREATE TABLE discount_offers(
do_id INT IDENTITY(1,1) PRIMARY KEY,
do_title VARCHAR(50) NOT NULL,
do_desc VARCHAR(255) NOT NULL,
do_packageID INT NOT NULL,
do_percentage_off DECIMAL(18,4) Not Null,

CONSTRAINT fk_package_discount_offers FOREIGN KEY (do_packageID)
REFERENCES package(p_id),
);


CREATE TABLE customer_package(
cp_id INT IDENTITY(1,1),
cp_cid INT NOT NULL,
cp_pid INT NOT NULL

CONSTRAINT fk_customer_package FOREIGN KEY (cp_cid)
REFERENCES customer(customer_id),

CONSTRAINT fk_package_customer FOREIGN KEY (cp_pid)
REFERENCES package(p_id),

primary key (cp_id,cp_cid, cp_pid)

);

CREATE TABLE department(
d_id INT IDENTITY(1,1) PRIMARY KEY,
d_name VARCHAR(50) NOT NULL
);

CREATE TABLE memo(
m_id INT IDENTITY(1,1) PRIMARY KEY,
m_title VARCHAR(50) NOT NULL,
m_desc VARCHAR(50) NOT NULL,
m_department INT NOT NULL,

CONSTRAINT fk_memo_department FOREIGN KEY (m_department)
REFERENCES department(d_id)
);


CREATE TABLE budget(
b_id INT IDENTITY(1,1) PRIMARY KEY,
b_month DATETIME NOT NULL DEFAULT GETDATE(),
b_value DECIMAL(18,4) NOT NULL,
);

CREATE TABLE marketing_campaign(
mc_id INT IDENTITY(1,1) PRIMARY KEY,
mc_title VARCHAR(50) NOT NULL,
mc_desc VARCHAR(255) NOT NULL,
mc_approval_status BIT DEFAULT 0,
mc_budget INT NOT NULL,
mc_department INT NOT NULL, 
mc_DATETIME DATETIME DEFAULT GETDATE(),


CONSTRAINT fk_marketing_department FOREIGN KEY (mc_department)
REFERENCES department(d_id)
);

CREATE TABLE employee(
e_id INT IDENTITY(1,1) PRIMARY KEY,
e_name VARCHAR(50) NOT NULL,
e_email VARCHAR(50) NOT NULL,
e_contact VARCHAR(50) NOT NULL,
userName VARCHAR(50) NOT NULL,
userPassword VARCHAR(50) NOT NULL,
userRole INT NOT NULL,
e_dID INT NOT NULL,

CONSTRAINT fk_employee_department FOREIGN KEY (e_dID)
REFERENCES department(d_id)
);


CREATE TABLE customer_complaint(
cc_id INT IDENTITY(1,1) PRIMARY KEY,
cc_title VARCHAR(50) NOT NULL,
cc_desc VARCHAR(255) NOT NULL,
cc_department INT NOT NULL, 
cc_DATETIME DATETIME NOT NULL DEFAULT GETDATE(),
cc_cid INT NOT NULL,

CONSTRAINT fk_customer_complaint FOREIGN KEY (cc_cid)
REFERENCES customer(customer_id),

CONSTRAINT fk_complaint_department FOREIGN KEY (cc_department)
REFERENCES department(d_id)
);

CREATE TABLE customer_feedback(
cfb_id INT IDENTITY(1,1) PRIMARY KEY,
cfb_title VARCHAR(50) NOT NULL,
cfb_desc VARCHAR(255) NOT NULL,
cfb_DATETIME DATETIME NOT NULL DEFAULT GETDATE(),
cfb_cid INT NOT NULL,

CONSTRAINT fk_customer_feedback FOREIGN KEY (cfb_cid)
REFERENCES customer(customer_id),
);

