--++++++++++++++++++++++++++++++++++++++++++
--		login_view
--++++++++++++++++++++++++++++++++++++++++++


CREATE VIEW login_view as
SELECT c.customer_id, c.userName,c.userPassword,c.userRole FROM customer c 
UNION ALL
SELECT e.e_id,e.userName,e.userPassword,e.userRole FROM employee e

--++++++++++++++++++++++++++++++++++++++++++
--	    single_customer_info 
--++++++++++++++++++++++++++++++++++++++++++


CREATE VIEW single_customer_info AS
SELECT c.customer_id as 'Customer ID',c.customer_name as 'Customer Name',c.customer_email as 'Customer Email',c.customer_dob as 'Customer DOB',c.userName as 'User Name',c.userPassword as 'Password',c.customer_joinDate as 'Join Date' FROM customer c



--++++++++++++++++++++++++++++++++++++++++++
--	      call_Record_view
--++++++++++++++++++++++++++++++++++++++++++

CREATE VIEW call_Record_view AS
SELECT  c1.customer_name AS 'Call From Name',
	mn1.mn_number AS 'Call From Number',
	c.customer_name AS 'Call To Name',
	mn.mn_number AS 'Call To Number',
	cr.call_duration AS 'Call Duration', 
	cup.cup_unit_charges AS 'Call Unit Charges', 
	call_duration*cup_unit_charges AS 'Call Charges' 
	FROM call_record cr 	
	INNER JOIN customer c
	ON c.customer_id=cr.call_to
	INNER JOIN registered_number rn
	ON rn.customerID=c.customer_id
	INNER JOIN mobile_numbers mn
	ON rn.mobile_number=mn.mn_number
	INNER JOIN customer c1
	ON c1.customer_id=cr.call_from
	INNER JOIN registered_number rn1
	ON rn1.customerID=c1.customer_id
	INNER JOIN mobile_numbers mn1
	ON rn1.mobile_number=mn1.mn_number
	INNER JOIN call_unit_price cup 
	ON cr.call_unit_charges = cup.cup_id

--++++++++++++++++++++++++++++++++++++++++++
--	      	 Sales_view 
--++++++++++++++++++++++++++++++++++++++++++


CREATE VIEW Sales_view AS
Select ssi.ssi_mobileNumber AS 'Mobile Number',c.customer_name AS 'Customer Name',sp.sp_sim_price AS 'Sim Price',ssi.ssi_DateTime AS 'Date of Sale' FROM sim_sale_invoice ssi
	INNER JOIN customer c 
	ON ssi.ssi_cid=c.customer_id
	INNER JOIN sim_price sp 
	ON sp.sp_id=ssi.ssi_sim_price

--++++++++++++++++++++++++++++++++++++++++++
--	      	 Sales_view 
--++++++++++++++++++++++++++++++++++++++++++


CREATE VIEW Memo_View AS
SELECt e.userName AS 'User Name',m.m_title AS 'Title',m.m_desc AS 'Description',d.d_name AS 'Department' FROM memo m
	INNER JOIN employee e 
	ON e.e_dID=m.m_department
	INNER JOIN department d
	ON m.m_department=d.d_id	

--++++++++++++++++++++++++++++++++++++++++++
--	       Marketing_view 
--++++++++++++++++++++++++++++++++++++++++++


CREATE VIEW Marketing_view AS
SELECT mc.mc_title AS 'Title',mc.mc_desc AS 'Description',mc.mc_approval_status AS 'Approval Status',mc.mc_budget AS 'Budget',d.d_name AS 'Department',mc.mc_DATETIME AS 'Date' FROM marketing_campaign mc
	INNER JOIN department d
	ON mc.mc_department=d.d_id

--++++++++++++++++++++++++++++++++++++++++++
--	      ALL_CustomerInfo_View 
--++++++++++++++++++++++++++++++++++++++++++


CREATE VIEW ALL_CustomerInfo_View AS
SELECT c.customer_id AS 'Customer ID',c.customer_name AS 'Customer Name',rn.mobile_number AS 'Mobile Number',c.customer_nic AS 'Customer NIC',c.customer_dob AS 'Customer DOB',c.userName AS 'User Name',c.customer_joinDate AS 'Join Date' FROM customer c
	INNER JOIN registered_number rn
	ON rn.customerID=c.customer_id

--++++++++++++++++++++++++++++++++++++++++++
--	       Complaint_view 
--++++++++++++++++++++++++++++++++++++++++++


CREATE VIEW Complaint_view AS
SELECT c.userName AS 'Customer User Name',cc.cc_title AS 'Complaint Title',cc.cc_desc AS 'Complaint Description',d.d_name AS 'Department',cc.cc_DATETIME AS 'Date' FROM customer_complaint cc
	INNER JOIN customer c 
	ON c.customer_id=cc.ccustomer_id
	INNER JOIN department d
	ON d.d_id=cc.cc_department

--++++++++++++++++++++++++++++++++++++++++++
--	       Feedback_view 
--++++++++++++++++++++++++++++++++++++++++++


CREATE VIEW Feedback_view AS
SELECT c.userName AS 'Customer User Name',cf.cfb_title AS 'Feedback Title',cf.cfb_desc AS 'Feedback Description',cf.cfb_DATETIME AS 'Date' FROM customer_feedback cf
	INNER JOIN customer c
	ON cf.cfb_cid=c.customer_id

--++++++++++++++++++++++++++++++++++++++++++
--	       Billing_Record 
--++++++++++++++++++++++++++++++++++++++++++


CREATE VIEW Billing_Record AS
SELECT  c1.userName AS 'User Name',
	mn1.mn_number AS 'Number',
	SUM(call_duration*cup_unit_charges) AS 'Total Bill' 
	FROM call_record cr 	
	INNER JOIN customer c
	ON c.customer_id=cr.call_to
	INNER JOIN registered_number rn
	ON rn.customerID=c.customer_id
	INNER JOIN mobile_numbers mn
	ON rn.mobile_number=mn.mn_number
	INNER JOIN customer c1
	ON c1.customer_id=cr.call_from
	INNER JOIN registered_number rn1
	ON rn1.customerID=c1.customer_id
	INNER JOIN mobile_numbers mn1
	ON rn1.mobile_number=mn1.mn_number
	INNER JOIN call_unit_price cup 
	ON cr.call_unit_charges = cup.cup_id
	GROUP BY  c1.userName,
	mn1.mn_number
	

--++++++++++++++++++++++++++++++++++++++++++
--	    customer_package_view 
--++++++++++++++++++++++++++++++++++++++++++


CREATE VIEW customer_package_view AS
SELECT c.userName AS 'User Name',p.p_title AS 'Package Title',p.p_desc AS 'Package Description' FROM customer_package cp 
	INNER JOIN package p 
	ON cp.cp_pid=p.p_id
	INNER JOIN customer c
	ON c.customer_id=cp.cp_cid

--++++++++++++++++++++++++++++++++++++++++++
--	    discount_offer_view 
--++++++++++++++++++++++++++++++++++++++++++
CREATE VIEW discount_offer_view AS
SELECT df.do_title AS 'Discount Offer Title',df.do_desc AS 'Discount Offer Description',p.p_title AS 'Package Title',df.do_percentage_off AS '% OFF '  FROM discount_offers df
	INNER JOIN package p
	ON df.do_packageID=p.p_id

--++++++++++++++++++++++++++++++++++++++++++
--	    all_package_info  
--++++++++++++++++++++++++++++++++++++++++++
CREATE VIEW all_package_info AS
SELECT p.p_id AS 'Package ID',p.p_title AS 'Package Title',p.p_desc AS 'Package Description' FROM package p

--++++++++++++++++++++++++++++++++++++++++++
--	    best_customer_view   
--++++++++++++++++++++++++++++++++++++++++++
CREATE VIEW best_customer_view AS
SELECT TOP 100 * FROM Billing_Record ORDER BY [Total Bill] DESC

--++++++++++++++++++++++++++++++++++++++++++
--	    worst_customer_view   
--++++++++++++++++++++++++++++++++++++++++++
CREATE VIEW worst_customer_view AS
SELECT TOP 100 * FROM Billing_Record ORDER BY [Total Bill]

--++++++++++++++++++++++++++++++++++++++++++
--	    best_package_view   
--++++++++++++++++++++++++++++++++++++++++++
CREATE VIEW best_package_view AS
SELECT TOP 10 p.p_title AS 'Package Title' ,COUNT(p.p_title) AS 'Customer Count' FROM customer_package cp 
	INNER JOIN package p 
	ON cp.cp_pid=p.p_id
	INNER JOIN customer c
	ON c.customer_id=cp.cp_cid
	GROUP BY p.p_title
	ORDER BY [Customer Count] DESC

--++++++++++++++++++++++++++++++++++++++++++
--	    worst_package_view   
--++++++++++++++++++++++++++++++++++++++++++
CREATE VIEW worst_package_view AS	
SELECT TOP 10 p.p_title AS 'Package Title' ,COUNT(p.p_title) AS 'Customer Count' FROM customer_package cp 
	INNER JOIN package p 
	ON cp.cp_pid=p.p_id
	INNER JOIN customer c
	ON c.customer_id=cp.cp_cid
	GROUP BY p.p_title
	ORDER BY [Customer Count]

