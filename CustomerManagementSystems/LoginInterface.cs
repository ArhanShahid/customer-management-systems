﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CustomerManagementSystems
{
    public partial class LoginInterface : Form
    {
        public LoginInterface()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void login_userName_TextChanged(object sender, EventArgs e)
        {

        }

        private void login_Password_TextChanged(object sender, EventArgs e)
        {

        }

       
        private void login_loginbtn_Click(object sender, EventArgs e)
        {
            string username = login_userName.Text;
            string password = login_Password.Text;

            loginManager lm = new loginManager();

            int role =  lm.authentication(username, password);

            switch (role)
            {
                case 1:
                    CustomerInterface cdb = new CustomerInterface();
                    this.Hide();
                    cdb.Show();
                    //SuccessInterface su1 = new SuccessInterface("Login Successful");
                    //su1.Show();
                    break;
               
                case 2:
                    OfficeManagerInterface omd = new OfficeManagerInterface();
                    this.Hide();
                    omd.Show();
                    //SuccessInterface su2 = new SuccessInterface("Login Successful");
                    //su2.Show();
                    break;

                case 3:
                     MarketingManagerInterface mmd = new MarketingManagerInterface();
                    this.Hide();
                    mmd.Show();
                    //SuccessInterface su3 = new SuccessInterface("Login Successful");
                    //su3.Show();
                    break;

                case 4:
                    CustomerSupportRepresentativeInterface csrd = new CustomerSupportRepresentativeInterface();
                    this.Hide();
                    csrd.Show();
                    //SuccessInterface su4 = new SuccessInterface("Login Successful");
                    //su4.Show();
                    break;
                case 5:
                    CEOInterface ceod = new CEOInterface();
                    this.Hide();
                    ceod.Show();
                    //SuccessInterface su5 = new SuccessInterface("Login Successful");
                    //su5.Show();
                    break;
               
                default:

                ErrorInterface er = new ErrorInterface("Oops !! Login Failed");
                er.Show();

                    break;
            }


            //if (username == "1" && password == "1")
            //{
                
               
            //}
            //else if (username == "2" && password == "2")
            //{
               
            //}
            //else if (username == "3" && password == "3")
            //{
               
            //}
            //else if (username == "4" && password == "4")
            //{
             
            //}
            //else if (username == "5" && password == "5")
            //{
                
            //}
            //else
            //{
            //    error er = new error("Oops !! Login Failed");
            //    er.Show();

            //}
           

            //MessageBox.Show(login_userName.Text + " " + login_Password.Text);
        }

        private void login_Load(object sender, EventArgs e)
        {

        }

    }
}
