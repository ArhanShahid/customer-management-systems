﻿namespace CustomerManagementSystems
{
    partial class CustomerSupportRepresentativeInterface
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CustomerSupportRepresentativeInterface));
            this.logoutBtn = new System.Windows.Forms.Button();
            this.Exit = new System.Windows.Forms.Button();
            this.ViewMemoBtn = new System.Windows.Forms.Button();
            this.ComplaintBtn = new System.Windows.Forms.Button();
            this.FeedbackBtn = new System.Windows.Forms.Button();
            this.CustomerInformationBtn = new System.Windows.Forms.Button();
            this.CallRecordsBtn = new System.Windows.Forms.Button();
            this.BillingRecordsBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // logoutBtn
            // 
            this.logoutBtn.Location = new System.Drawing.Point(512, 55);
            this.logoutBtn.Name = "logoutBtn";
            this.logoutBtn.Size = new System.Drawing.Size(75, 25);
            this.logoutBtn.TabIndex = 0;
            this.logoutBtn.Text = "Logout";
            this.logoutBtn.UseVisualStyleBackColor = true;
            this.logoutBtn.Click += new System.EventHandler(this.logoutBtn_Click);
            // 
            // Exit
            // 
            this.Exit.Location = new System.Drawing.Point(512, 82);
            this.Exit.Name = "Exit";
            this.Exit.Size = new System.Drawing.Size(75, 25);
            this.Exit.TabIndex = 1;
            this.Exit.Text = "Exit";
            this.Exit.UseVisualStyleBackColor = true;
            this.Exit.Click += new System.EventHandler(this.Exit_Click);
            // 
            // ViewMemoBtn
            // 
            this.ViewMemoBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(85)))), ((int)(((byte)(85)))));
            this.ViewMemoBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ViewMemoBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ViewMemoBtn.ForeColor = System.Drawing.Color.White;
            this.ViewMemoBtn.Location = new System.Drawing.Point(45, 80);
            this.ViewMemoBtn.Name = "ViewMemoBtn";
            this.ViewMemoBtn.Size = new System.Drawing.Size(150, 35);
            this.ViewMemoBtn.TabIndex = 4;
            this.ViewMemoBtn.Text = "Memo";
            this.ViewMemoBtn.UseVisualStyleBackColor = false;
            this.ViewMemoBtn.Click += new System.EventHandler(this.ViewMemoBtn_Click);
            // 
            // ComplaintBtn
            // 
            this.ComplaintBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(85)))), ((int)(((byte)(85)))));
            this.ComplaintBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ComplaintBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ComplaintBtn.ForeColor = System.Drawing.Color.White;
            this.ComplaintBtn.Location = new System.Drawing.Point(45, 240);
            this.ComplaintBtn.Name = "ComplaintBtn";
            this.ComplaintBtn.Size = new System.Drawing.Size(150, 35);
            this.ComplaintBtn.TabIndex = 5;
            this.ComplaintBtn.Text = "Complaint";
            this.ComplaintBtn.UseVisualStyleBackColor = false;
            this.ComplaintBtn.Click += new System.EventHandler(this.ComplaintBtn_Click);
            // 
            // FeedbackBtn
            // 
            this.FeedbackBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(85)))), ((int)(((byte)(85)))));
            this.FeedbackBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.FeedbackBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FeedbackBtn.ForeColor = System.Drawing.Color.White;
            this.FeedbackBtn.Location = new System.Drawing.Point(45, 280);
            this.FeedbackBtn.Name = "FeedbackBtn";
            this.FeedbackBtn.Size = new System.Drawing.Size(150, 35);
            this.FeedbackBtn.TabIndex = 6;
            this.FeedbackBtn.Text = "Feedback";
            this.FeedbackBtn.UseVisualStyleBackColor = false;
            this.FeedbackBtn.Click += new System.EventHandler(this.FeedbackBtn_Click);
            // 
            // CustomerInformationBtn
            // 
            this.CustomerInformationBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(85)))), ((int)(((byte)(85)))));
            this.CustomerInformationBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CustomerInformationBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CustomerInformationBtn.ForeColor = System.Drawing.Color.White;
            this.CustomerInformationBtn.Location = new System.Drawing.Point(45, 120);
            this.CustomerInformationBtn.Name = "CustomerInformationBtn";
            this.CustomerInformationBtn.Size = new System.Drawing.Size(150, 35);
            this.CustomerInformationBtn.TabIndex = 7;
            this.CustomerInformationBtn.Text = "Customer Information";
            this.CustomerInformationBtn.UseVisualStyleBackColor = false;
            this.CustomerInformationBtn.Click += new System.EventHandler(this.CustomerInformationBtn_Click);
            // 
            // CallRecordsBtn
            // 
            this.CallRecordsBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(85)))), ((int)(((byte)(85)))));
            this.CallRecordsBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CallRecordsBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CallRecordsBtn.ForeColor = System.Drawing.Color.White;
            this.CallRecordsBtn.Location = new System.Drawing.Point(45, 160);
            this.CallRecordsBtn.Name = "CallRecordsBtn";
            this.CallRecordsBtn.Size = new System.Drawing.Size(150, 35);
            this.CallRecordsBtn.TabIndex = 8;
            this.CallRecordsBtn.Text = "Call Records";
            this.CallRecordsBtn.UseVisualStyleBackColor = false;
            this.CallRecordsBtn.Click += new System.EventHandler(this.CallRecordsBtn_Click);
            // 
            // BillingRecordsBtn
            // 
            this.BillingRecordsBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(85)))), ((int)(((byte)(85)))));
            this.BillingRecordsBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BillingRecordsBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BillingRecordsBtn.ForeColor = System.Drawing.Color.White;
            this.BillingRecordsBtn.Location = new System.Drawing.Point(45, 200);
            this.BillingRecordsBtn.Name = "BillingRecordsBtn";
            this.BillingRecordsBtn.Size = new System.Drawing.Size(150, 35);
            this.BillingRecordsBtn.TabIndex = 9;
            this.BillingRecordsBtn.Text = "Billing Records";
            this.BillingRecordsBtn.UseVisualStyleBackColor = false;
            this.BillingRecordsBtn.Click += new System.EventHandler(this.BillingRecordsBtn_Click);
            // 
            // customerSupportRepresentativeDashboard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::CustomerManagementSystems.Properties.Resources.role;
            this.ClientSize = new System.Drawing.Size(599, 396);
            this.Controls.Add(this.BillingRecordsBtn);
            this.Controls.Add(this.CallRecordsBtn);
            this.Controls.Add(this.CustomerInformationBtn);
            this.Controls.Add(this.FeedbackBtn);
            this.Controls.Add(this.ComplaintBtn);
            this.Controls.Add(this.ViewMemoBtn);
            this.Controls.Add(this.Exit);
            this.Controls.Add(this.logoutBtn);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "customerSupportRepresentativeDashboard";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Dashboard - Customer Support Representative";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button logoutBtn;
        private System.Windows.Forms.Button Exit;
        private System.Windows.Forms.Button ViewMemoBtn;
        private System.Windows.Forms.Button ComplaintBtn;
        private System.Windows.Forms.Button FeedbackBtn;
        private System.Windows.Forms.Button CustomerInformationBtn;
        private System.Windows.Forms.Button CallRecordsBtn;
        private System.Windows.Forms.Button BillingRecordsBtn;
    }
}