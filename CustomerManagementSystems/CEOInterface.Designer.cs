﻿namespace CustomerManagementSystems
{
    partial class CEOInterface
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CEOInterface));
            this.logoutBtn = new System.Windows.Forms.Button();
            this.exitBtn = new System.Windows.Forms.Button();
            this.BillingRecordsBtn = new System.Windows.Forms.Button();
            this.CallRecordsBtn = new System.Windows.Forms.Button();
            this.CustomerInformationBtn = new System.Windows.Forms.Button();
            this.ComplaintBtn = new System.Windows.Forms.Button();
            this.ViewMemoBtn = new System.Windows.Forms.Button();
            this.ViewSalesBtn = new System.Windows.Forms.Button();
            this.bestCustomers = new System.Windows.Forms.Button();
            this.worstCustomer = new System.Windows.Forms.Button();
            this.bestPackages = new System.Windows.Forms.Button();
            this.worstPackages = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // logoutBtn
            // 
            this.logoutBtn.Location = new System.Drawing.Point(512, 55);
            this.logoutBtn.Name = "logoutBtn";
            this.logoutBtn.Size = new System.Drawing.Size(75, 25);
            this.logoutBtn.TabIndex = 0;
            this.logoutBtn.Text = "Logout";
            this.logoutBtn.UseVisualStyleBackColor = true;
            this.logoutBtn.Click += new System.EventHandler(this.logoutBtn_Click);
            // 
            // exitBtn
            // 
            this.exitBtn.Location = new System.Drawing.Point(512, 82);
            this.exitBtn.Name = "exitBtn";
            this.exitBtn.Size = new System.Drawing.Size(75, 25);
            this.exitBtn.TabIndex = 1;
            this.exitBtn.Text = "Exit";
            this.exitBtn.UseVisualStyleBackColor = true;
            this.exitBtn.Click += new System.EventHandler(this.exitBtn_Click);
            // 
            // BillingRecordsBtn
            // 
            this.BillingRecordsBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(85)))), ((int)(((byte)(85)))));
            this.BillingRecordsBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BillingRecordsBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BillingRecordsBtn.ForeColor = System.Drawing.Color.White;
            this.BillingRecordsBtn.Location = new System.Drawing.Point(45, 200);
            this.BillingRecordsBtn.Name = "BillingRecordsBtn";
            this.BillingRecordsBtn.Size = new System.Drawing.Size(150, 35);
            this.BillingRecordsBtn.TabIndex = 14;
            this.BillingRecordsBtn.Text = "Billing Records";
            this.BillingRecordsBtn.UseVisualStyleBackColor = false;
            this.BillingRecordsBtn.Click += new System.EventHandler(this.BillingRecordsBtn_Click);
            // 
            // CallRecordsBtn
            // 
            this.CallRecordsBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(85)))), ((int)(((byte)(85)))));
            this.CallRecordsBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CallRecordsBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CallRecordsBtn.ForeColor = System.Drawing.Color.White;
            this.CallRecordsBtn.Location = new System.Drawing.Point(45, 160);
            this.CallRecordsBtn.Name = "CallRecordsBtn";
            this.CallRecordsBtn.Size = new System.Drawing.Size(150, 35);
            this.CallRecordsBtn.TabIndex = 13;
            this.CallRecordsBtn.Text = "Call Records";
            this.CallRecordsBtn.UseVisualStyleBackColor = false;
            this.CallRecordsBtn.Click += new System.EventHandler(this.CallRecordsBtn_Click);
            // 
            // CustomerInformationBtn
            // 
            this.CustomerInformationBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(85)))), ((int)(((byte)(85)))));
            this.CustomerInformationBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CustomerInformationBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CustomerInformationBtn.ForeColor = System.Drawing.Color.White;
            this.CustomerInformationBtn.Location = new System.Drawing.Point(45, 120);
            this.CustomerInformationBtn.Name = "CustomerInformationBtn";
            this.CustomerInformationBtn.Size = new System.Drawing.Size(150, 35);
            this.CustomerInformationBtn.TabIndex = 12;
            this.CustomerInformationBtn.Text = "Customer Information";
            this.CustomerInformationBtn.UseVisualStyleBackColor = false;
            this.CustomerInformationBtn.Click += new System.EventHandler(this.CustomerInformationBtn_Click);
            // 
            // ComplaintBtn
            // 
            this.ComplaintBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(85)))), ((int)(((byte)(85)))));
            this.ComplaintBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ComplaintBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ComplaintBtn.ForeColor = System.Drawing.Color.White;
            this.ComplaintBtn.Location = new System.Drawing.Point(45, 240);
            this.ComplaintBtn.Name = "ComplaintBtn";
            this.ComplaintBtn.Size = new System.Drawing.Size(150, 35);
            this.ComplaintBtn.TabIndex = 11;
            this.ComplaintBtn.Text = "Compaign";
            this.ComplaintBtn.UseVisualStyleBackColor = false;
            this.ComplaintBtn.Click += new System.EventHandler(this.ComplaintBtn_Click);
            // 
            // ViewMemoBtn
            // 
            this.ViewMemoBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(85)))), ((int)(((byte)(85)))));
            this.ViewMemoBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ViewMemoBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ViewMemoBtn.ForeColor = System.Drawing.Color.White;
            this.ViewMemoBtn.Location = new System.Drawing.Point(45, 80);
            this.ViewMemoBtn.Name = "ViewMemoBtn";
            this.ViewMemoBtn.Size = new System.Drawing.Size(150, 35);
            this.ViewMemoBtn.TabIndex = 10;
            this.ViewMemoBtn.Text = "Memo";
            this.ViewMemoBtn.UseVisualStyleBackColor = false;
            this.ViewMemoBtn.Click += new System.EventHandler(this.ViewMemoBtn_Click);
            // 
            // ViewSalesBtn
            // 
            this.ViewSalesBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(85)))), ((int)(((byte)(85)))));
            this.ViewSalesBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ViewSalesBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ViewSalesBtn.ForeColor = System.Drawing.Color.White;
            this.ViewSalesBtn.Location = new System.Drawing.Point(45, 280);
            this.ViewSalesBtn.Name = "ViewSalesBtn";
            this.ViewSalesBtn.Size = new System.Drawing.Size(150, 35);
            this.ViewSalesBtn.TabIndex = 15;
            this.ViewSalesBtn.Text = "View Sales";
            this.ViewSalesBtn.UseVisualStyleBackColor = false;
            this.ViewSalesBtn.Click += new System.EventHandler(this.ViewSalesBtn_Click);
            // 
            // bestCustomers
            // 
            this.bestCustomers.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(85)))), ((int)(((byte)(85)))));
            this.bestCustomers.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bestCustomers.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bestCustomers.ForeColor = System.Drawing.Color.White;
            this.bestCustomers.Location = new System.Drawing.Point(201, 80);
            this.bestCustomers.Name = "bestCustomers";
            this.bestCustomers.Size = new System.Drawing.Size(150, 35);
            this.bestCustomers.TabIndex = 16;
            this.bestCustomers.Text = "Best Customers";
            this.bestCustomers.UseVisualStyleBackColor = false;
            this.bestCustomers.Click += new System.EventHandler(this.bestCustomers_Click);
            // 
            // worstCustomer
            // 
            this.worstCustomer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(85)))), ((int)(((byte)(85)))));
            this.worstCustomer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.worstCustomer.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.worstCustomer.ForeColor = System.Drawing.Color.White;
            this.worstCustomer.Location = new System.Drawing.Point(201, 121);
            this.worstCustomer.Name = "worstCustomer";
            this.worstCustomer.Size = new System.Drawing.Size(150, 35);
            this.worstCustomer.TabIndex = 17;
            this.worstCustomer.Text = "Worst Customers";
            this.worstCustomer.UseVisualStyleBackColor = false;
            this.worstCustomer.Click += new System.EventHandler(this.worstCustomer_Click);
            // 
            // bestPackages
            // 
            this.bestPackages.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(85)))), ((int)(((byte)(85)))));
            this.bestPackages.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bestPackages.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bestPackages.ForeColor = System.Drawing.Color.White;
            this.bestPackages.Location = new System.Drawing.Point(201, 162);
            this.bestPackages.Name = "bestPackages";
            this.bestPackages.Size = new System.Drawing.Size(150, 35);
            this.bestPackages.TabIndex = 18;
            this.bestPackages.Text = "Best Packages";
            this.bestPackages.UseVisualStyleBackColor = false;
            this.bestPackages.Click += new System.EventHandler(this.bestPackages_Click);
            // 
            // worstPackages
            // 
            this.worstPackages.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(85)))), ((int)(((byte)(85)))));
            this.worstPackages.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.worstPackages.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.worstPackages.ForeColor = System.Drawing.Color.White;
            this.worstPackages.Location = new System.Drawing.Point(201, 203);
            this.worstPackages.Name = "worstPackages";
            this.worstPackages.Size = new System.Drawing.Size(150, 35);
            this.worstPackages.TabIndex = 19;
            this.worstPackages.Text = "Worst Packages";
            this.worstPackages.UseVisualStyleBackColor = false;
            this.worstPackages.Click += new System.EventHandler(this.worstPackages_Click);
            // 
            // CEOInterface
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::CustomerManagementSystems.Properties.Resources.role;
            this.ClientSize = new System.Drawing.Size(599, 396);
            this.Controls.Add(this.worstPackages);
            this.Controls.Add(this.bestPackages);
            this.Controls.Add(this.worstCustomer);
            this.Controls.Add(this.bestCustomers);
            this.Controls.Add(this.ViewSalesBtn);
            this.Controls.Add(this.BillingRecordsBtn);
            this.Controls.Add(this.CallRecordsBtn);
            this.Controls.Add(this.CustomerInformationBtn);
            this.Controls.Add(this.ComplaintBtn);
            this.Controls.Add(this.ViewMemoBtn);
            this.Controls.Add(this.exitBtn);
            this.Controls.Add(this.logoutBtn);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "CEOInterface";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CEO - Dashboard";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button logoutBtn;
        private System.Windows.Forms.Button exitBtn;
        private System.Windows.Forms.Button BillingRecordsBtn;
        private System.Windows.Forms.Button CallRecordsBtn;
        private System.Windows.Forms.Button CustomerInformationBtn;
        private System.Windows.Forms.Button ComplaintBtn;
        private System.Windows.Forms.Button ViewMemoBtn;
        private System.Windows.Forms.Button ViewSalesBtn;
        private System.Windows.Forms.Button bestCustomers;
        private System.Windows.Forms.Button worstCustomer;
        private System.Windows.Forms.Button bestPackages;
        private System.Windows.Forms.Button worstPackages;
    }
}