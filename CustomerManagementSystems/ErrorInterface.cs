﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CustomerManagementSystems
{
    public partial class ErrorInterface : Form
    {
        public ErrorInterface(string message)
        {
            InitializeComponent();
            errorMessage.Text = message;
        }

        private void error_Load(object sender, EventArgs e)
        {

        }

        private void errorBtn_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void errorMessage_Click(object sender, EventArgs e)
        {

        }
    }
}
