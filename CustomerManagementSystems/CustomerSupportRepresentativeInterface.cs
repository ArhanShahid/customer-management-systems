﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace CustomerManagementSystems
{
    public partial class CustomerSupportRepresentativeInterface : Form
    {
        public CustomerSupportRepresentativeInterface()
        {
            InitializeComponent();
        }

        private void logoutBtn_Click(object sender, EventArgs e)
        {
            this.Hide();
            LoginInterface log = new LoginInterface();
            log.Show();

        }

        private void Exit_Click(object sender, EventArgs e)
        {
            if (File.Exists("info.txt"))
            {
                File.Delete("info.txt");
            }
            Application.Exit();
        }

        private void ViewMemoBtn_Click(object sender, EventArgs e)
        {
            CustomerSupportRepresentativeManager csrm = new CustomerSupportRepresentativeManager();
            MemoInterface mi = new MemoInterface();
            mi.MemoDataGridView.DataSource = csrm.getMemo();
            this.Close();
            mi.Show();
        }

        private void ComplaintBtn_Click(object sender, EventArgs e)
        {
            CustomerSupportRepresentativeManager csrm = new CustomerSupportRepresentativeManager();
            ComplaintInterface ci = new ComplaintInterface();
            ci.ComplaintDataView.DataSource = csrm.getCamplaint();
            this.Close();
            ci.Show();

        }

        private void BillingRecordsBtn_Click(object sender, EventArgs e)
        {
            CustomerSupportRepresentativeManager csr = new CustomerSupportRepresentativeManager();
            BillingInterface bi = new BillingInterface();
            bi.BillingDataGridView.DataSource = csr.getBillingRecords();
            this.Close();
            bi.Show();
        }

        private void CallRecordsBtn_Click(object sender, EventArgs e)
        {
            CustomerSupportRepresentativeManager csr = new CustomerSupportRepresentativeManager();
            CallRecordsInterface cri = new CallRecordsInterface();
            cri.CustomerCallRDataGridView.DataSource = csr.getCustomerCallRecords();
            this.Close();
            cri.Show();
        }

        private void FeedbackBtn_Click(object sender, EventArgs e)
        {
            CustomerSupportRepresentativeManager csrm = new CustomerSupportRepresentativeManager();
            FeedBackInterface fbi = new FeedBackInterface();
            fbi.FeedBackDataView.DataSource = csrm.getFeedback();
            this.Close();
            fbi.Show();
        }

        private void CustomerInformationBtn_Click(object sender, EventArgs e)
        {
            CustomerSupportRepresentativeManager csrm = new CustomerSupportRepresentativeManager();
            AllCustomerInfoInterface acii = new AllCustomerInfoInterface();
            acii.AllCustomerDataGridView.DataSource = csrm.getAllCustomerInfo();
            this.Close();
            acii.Show();
        }
    }
}
