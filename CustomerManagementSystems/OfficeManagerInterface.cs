﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace CustomerManagementSystems
{
    public partial class OfficeManagerInterface : Form
    {
        public OfficeManagerInterface()
        {
            InitializeComponent();
        }

        private void exitBtn_Click(object sender, EventArgs e)
        {
            if (File.Exists("info.txt"))
            {
                File.Delete("info.txt");
            }
            Application.Exit();
        }

        private void logoutBtn_Click(object sender, EventArgs e)
        {
            if (File.Exists("info.txt"))
            {
                File.Delete("info.txt");
            }
            this.Hide();
            LoginInterface log = new LoginInterface();
            log.Show();
        }

        private void ViewMemoBtn_Click(object sender, EventArgs e)
        {
            OfficeManager om = new OfficeManager();
            MemoInterface mi = new MemoInterface();
            mi.MemoDataGridView.DataSource = om.getMemo();
            this.Close();
            mi.Show();
        }

        private void CustomerInformationBtn_Click(object sender, EventArgs e)
        {
            OfficeManager om = new OfficeManager();
            AllCustomerInfoInterface acii = new AllCustomerInfoInterface();
            acii.AllCustomerDataGridView.DataSource = om.getAllCustomerInfo();
            this.Close();
            acii.Show();

        }

        private void CallRecordsBtn_Click(object sender, EventArgs e)
        {
            OfficeManager om = new OfficeManager();
            CallRecordsInterface cri = new CallRecordsInterface();
            cri.CustomerCallRDataGridView.DataSource = om.getCustomerCallRecords();
            this.Close();
            cri.Show();
        }

        private void BillingRecordsBtn_Click(object sender, EventArgs e)
        {
            OfficeManager om = new OfficeManager();
            BillingInterface bi = new BillingInterface();
            bi.BillingDataGridView.DataSource = om.getBillingRecords();
            this.Close();
            bi.Show();
        }

        private void ComplaintBtn_Click(object sender, EventArgs e)
        {
            OfficeManager om = new OfficeManager();
            CampaignInterface ci = new CampaignInterface();
            ci.CampaignDataGridView.DataSource = om.getCampaign();
            this.Close();
            ci.Show();
        }

        private void ViewSalesBtn_Click(object sender, EventArgs e)
        {
            OfficeManager om = new OfficeManager();
            SalesInterface si = new SalesInterface();
            si.SalesDataGridView.DataSource = om.getSalesRecords();
            this.Close();
            si.Show();
        }

        private void printRecords_Click(object sender, EventArgs e)
        {

        }

        private void managedPackagesBtn_Click(object sender, EventArgs e)
        {
            OfficeManager om = new OfficeManager();
            CustomerPackageInterface cpi = new CustomerPackageInterface();
            cpi.PackageDataGridView.DataSource = om.getPackages();
            this.Hide();
            cpi.Show();
        }

        private void SellNumberBtn_Click(object sender, EventArgs e)
        {
            OfficeManager om = new OfficeManager();
            SellNumberInterface sni = new SellNumberInterface();
            sni.ANumberDataGridView.DataSource = om.getAvailableNumber();
            this.Hide();
            sni.Show();
        }
    }
}
