﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace CustomerManagementSystems
{
    public partial class CEOInterface : Form
    {
        public CEOInterface()
        {
            InitializeComponent();
        }

        private void logoutBtn_Click(object sender, EventArgs e)
        {
            if (File.Exists("info.txt"))
            {
                File.Delete("info.txt");
            }
            this.Hide();
            LoginInterface log = new LoginInterface();
            log.Show();
        }

        private void exitBtn_Click(object sender, EventArgs e)
        {
            if (File.Exists("info.txt"))
            {
                File.Delete("info.txt");
            }
            Application.Exit();
        }

        private void ViewMemoBtn_Click(object sender, EventArgs e)
        {
            CEOManager ceom = new CEOManager();
            MemoInterface mi = new MemoInterface();
            mi.MemoDataGridView.DataSource = ceom.getMemo();
            this.Close();
            mi.Show();
        }

        private void CustomerInformationBtn_Click(object sender, EventArgs e)
        {
            CEOManager ceo = new CEOManager();
            AllCustomerInfoInterface acii = new AllCustomerInfoInterface();
            acii.AllCustomerDataGridView.DataSource = ceo.getAllCustomerInfo();
            this.Close();
            acii.Show();
        }

        private void CallRecordsBtn_Click(object sender, EventArgs e)
        {
            CEOManager ceo = new CEOManager();
            CallRecordsInterface cri = new CallRecordsInterface();
            cri.CustomerCallRDataGridView.DataSource = ceo.getCustomerCallRecords();
            this.Close();
            cri.Show();
        }

        private void BillingRecordsBtn_Click(object sender, EventArgs e)
        {
            CEOManager ceom = new CEOManager();
            BillingInterface bi = new BillingInterface();
            bi.BillingDataGridView.DataSource = ceom.getBillingRecords();
            this.Close();
            bi.Show();
        }

        private void ComplaintBtn_Click(object sender, EventArgs e)
        {
            CEOManager ceom = new CEOManager();
            CampaignInterface ci = new CampaignInterface();
            ci.CampaignDataGridView.DataSource = ceom.getCampaign();
            this.Close();
            ci.Show();
        }

        private void ViewSalesBtn_Click(object sender, EventArgs e)
        {
            CEOManager ceom = new CEOManager();
            SalesInterface si = new SalesInterface();
            si.SalesDataGridView.DataSource = ceom.getSalesRecords();
            this.Close();
            si.Show();
        }

        private void bestCustomers_Click(object sender, EventArgs e)
        {
            CEOManager ceom = new CEOManager();
            ReportInterface reportInterface = new ReportInterface();
            reportInterface.reportDataGridView.DataSource = ceom.getBestCustomers();
            reportInterface.reoprtTitleLabel.Text = "Best Customers";
            this.Close();
            reportInterface.Show();
        }

        private void worstCustomer_Click(object sender, EventArgs e)
        {
            CEOManager ceom = new CEOManager();
            ReportInterface reportInterface = new ReportInterface();
            reportInterface.reportDataGridView.DataSource = ceom.getWorstCustomers();
            reportInterface.reoprtTitleLabel.Text = "Worst Customers";
            this.Close();
            reportInterface.Show();
        }

        private void bestPackages_Click(object sender, EventArgs e)
        {
            CEOManager ceom = new CEOManager();
            ReportInterface reportInterface = new ReportInterface();
            reportInterface.reportDataGridView.DataSource = ceom.getBestPackages();
            reportInterface.reoprtTitleLabel.Text = "Best Packages";
            this.Close();
            reportInterface.Show();
        }

        private void worstPackages_Click(object sender, EventArgs e)
        {
            CEOManager ceom = new CEOManager();
            ReportInterface reportInterface = new ReportInterface();
            reportInterface.reportDataGridView.DataSource = ceom.getWorstPackages();
            reportInterface.reoprtTitleLabel.Text = "Worst Packages";
            this.Close();
            reportInterface.Show();
        }
    }
}
