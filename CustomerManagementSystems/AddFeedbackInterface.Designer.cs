﻿namespace CustomerManagementSystems
{
    partial class AddFeedbackInterface
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddFeedbackInterface));
            this.exitBtn = new System.Windows.Forms.Button();
            this.logoutBtn = new System.Windows.Forms.Button();
            this.FeedbackTitle = new System.Windows.Forms.Label();
            this.lableDescription = new System.Windows.Forms.Label();
            this.fbTitleTb = new System.Windows.Forms.TextBox();
            this.fbDescRTb = new System.Windows.Forms.RichTextBox();
            this.AddFeedback = new System.Windows.Forms.Button();
            this.backBTN = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // exitBtn
            // 
            this.exitBtn.Location = new System.Drawing.Point(512, 114);
            this.exitBtn.Name = "exitBtn";
            this.exitBtn.Size = new System.Drawing.Size(75, 25);
            this.exitBtn.TabIndex = 3;
            this.exitBtn.Text = "Exit";
            this.exitBtn.UseVisualStyleBackColor = true;
            this.exitBtn.Click += new System.EventHandler(this.exitBtn_Click);
            // 
            // logoutBtn
            // 
            this.logoutBtn.Location = new System.Drawing.Point(512, 87);
            this.logoutBtn.Name = "logoutBtn";
            this.logoutBtn.Size = new System.Drawing.Size(75, 25);
            this.logoutBtn.TabIndex = 2;
            this.logoutBtn.Text = "Logout";
            this.logoutBtn.UseVisualStyleBackColor = true;
            this.logoutBtn.Click += new System.EventHandler(this.logoutBtn_Click);
            // 
            // FeedbackTitle
            // 
            this.FeedbackTitle.AutoSize = true;
            this.FeedbackTitle.BackColor = System.Drawing.Color.White;
            this.FeedbackTitle.Location = new System.Drawing.Point(75, 94);
            this.FeedbackTitle.Name = "FeedbackTitle";
            this.FeedbackTitle.Size = new System.Drawing.Size(36, 13);
            this.FeedbackTitle.TabIndex = 4;
            this.FeedbackTitle.Text = "Title : ";
            // 
            // lableDescription
            // 
            this.lableDescription.AutoSize = true;
            this.lableDescription.BackColor = System.Drawing.Color.White;
            this.lableDescription.Location = new System.Drawing.Point(45, 142);
            this.lableDescription.Name = "lableDescription";
            this.lableDescription.Size = new System.Drawing.Size(66, 13);
            this.lableDescription.TabIndex = 5;
            this.lableDescription.Text = "Description :";
            // 
            // fbTitleTb
            // 
            this.fbTitleTb.Location = new System.Drawing.Point(117, 89);
            this.fbTitleTb.Name = "fbTitleTb";
            this.fbTitleTb.Size = new System.Drawing.Size(363, 20);
            this.fbTitleTb.TabIndex = 6;
            this.fbTitleTb.TextChanged += new System.EventHandler(this.fbTitleTb_TextChanged);
            // 
            // fbDescRTb
            // 
            this.fbDescRTb.Location = new System.Drawing.Point(117, 142);
            this.fbDescRTb.Name = "fbDescRTb";
            this.fbDescRTb.Size = new System.Drawing.Size(363, 164);
            this.fbDescRTb.TabIndex = 7;
            this.fbDescRTb.Text = "";
            // 
            // AddFeedback
            // 
            this.AddFeedback.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(85)))), ((int)(((byte)(85)))));
            this.AddFeedback.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.AddFeedback.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AddFeedback.ForeColor = System.Drawing.Color.White;
            this.AddFeedback.Location = new System.Drawing.Point(117, 330);
            this.AddFeedback.Name = "AddFeedback";
            this.AddFeedback.Size = new System.Drawing.Size(363, 35);
            this.AddFeedback.TabIndex = 11;
            this.AddFeedback.Text = "Add Feedback";
            this.AddFeedback.UseVisualStyleBackColor = false;
            this.AddFeedback.Click += new System.EventHandler(this.AddFeedback_Click);
            // 
            // backBTN
            // 
            this.backBTN.Location = new System.Drawing.Point(512, 60);
            this.backBTN.Name = "backBTN";
            this.backBTN.Size = new System.Drawing.Size(75, 25);
            this.backBTN.TabIndex = 20;
            this.backBTN.Text = "Back";
            this.backBTN.UseVisualStyleBackColor = true;
            this.backBTN.Click += new System.EventHandler(this.backBTN_Click);
            // 
            // AddFeedbackInterface
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::CustomerManagementSystems.Properties.Resources.role;
            this.ClientSize = new System.Drawing.Size(599, 396);
            this.Controls.Add(this.backBTN);
            this.Controls.Add(this.AddFeedback);
            this.Controls.Add(this.fbDescRTb);
            this.Controls.Add(this.fbTitleTb);
            this.Controls.Add(this.lableDescription);
            this.Controls.Add(this.FeedbackTitle);
            this.Controls.Add(this.exitBtn);
            this.Controls.Add(this.logoutBtn);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "AddFeedbackInterface";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AddFeedbackInterface";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button exitBtn;
        private System.Windows.Forms.Button logoutBtn;
        private System.Windows.Forms.Label FeedbackTitle;
        private System.Windows.Forms.Label lableDescription;
        private System.Windows.Forms.Button AddFeedback;
        public System.Windows.Forms.TextBox fbTitleTb;
        public System.Windows.Forms.RichTextBox fbDescRTb;
        private System.Windows.Forms.Button backBTN;
    }
}