﻿namespace CustomerManagementSystems
{
    partial class LoginInterface
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LoginInterface));
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.login_userName = new System.Windows.Forms.TextBox();
            this.login_Password = new System.Windows.Forms.TextBox();
            this.login_loginbtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(497, 326);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Exit";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(94, 164);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "User Name :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(104, 198);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Password :";
            // 
            // login_userName
            // 
            this.login_userName.Location = new System.Drawing.Point(178, 163);
            this.login_userName.Name = "login_userName";
            this.login_userName.Size = new System.Drawing.Size(286, 20);
            this.login_userName.TabIndex = 3;
            this.login_userName.TextChanged += new System.EventHandler(this.login_userName_TextChanged);
            // 
            // login_Password
            // 
            this.login_Password.Location = new System.Drawing.Point(178, 198);
            this.login_Password.Name = "login_Password";
            this.login_Password.PasswordChar = '*';
            this.login_Password.Size = new System.Drawing.Size(286, 20);
            this.login_Password.TabIndex = 4;
            this.login_Password.TextChanged += new System.EventHandler(this.login_Password_TextChanged);
            // 
            // login_loginbtn
            // 
            this.login_loginbtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(85)))), ((int)(((byte)(85)))));
            this.login_loginbtn.FlatAppearance.BorderSize = 0;
            this.login_loginbtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.login_loginbtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.login_loginbtn.ForeColor = System.Drawing.Color.White;
            this.login_loginbtn.Location = new System.Drawing.Point(178, 246);
            this.login_loginbtn.Name = "login_loginbtn";
            this.login_loginbtn.Size = new System.Drawing.Size(286, 34);
            this.login_loginbtn.TabIndex = 5;
            this.login_loginbtn.Text = "Login";
            this.login_loginbtn.UseVisualStyleBackColor = false;
            this.login_loginbtn.Click += new System.EventHandler(this.login_loginbtn_Click);
            // 
            // login
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::CustomerManagementSystems.Properties.Resources.login;
            this.ClientSize = new System.Drawing.Size(584, 361);
            this.Controls.Add(this.login_loginbtn);
            this.Controls.Add(this.login_Password);
            this.Controls.Add(this.login_userName);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "login";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Login";
            this.Load += new System.EventHandler(this.login_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox login_userName;
        private System.Windows.Forms.TextBox login_Password;
        private System.Windows.Forms.Button login_loginbtn;
    }
}