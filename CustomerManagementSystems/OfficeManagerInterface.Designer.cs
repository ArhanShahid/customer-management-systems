﻿namespace CustomerManagementSystems
{
    partial class OfficeManagerInterface
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OfficeManagerInterface));
            this.logoutBtn = new System.Windows.Forms.Button();
            this.exitBtn = new System.Windows.Forms.Button();
            this.ViewSalesBtn = new System.Windows.Forms.Button();
            this.BillingRecordsBtn = new System.Windows.Forms.Button();
            this.CallRecordsBtn = new System.Windows.Forms.Button();
            this.CustomerInformationBtn = new System.Windows.Forms.Button();
            this.ComplaintBtn = new System.Windows.Forms.Button();
            this.ViewMemoBtn = new System.Windows.Forms.Button();
            this.SellNumberBtn = new System.Windows.Forms.Button();
            this.managedPackagesBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // logoutBtn
            // 
            this.logoutBtn.Location = new System.Drawing.Point(512, 55);
            this.logoutBtn.Name = "logoutBtn";
            this.logoutBtn.Size = new System.Drawing.Size(75, 25);
            this.logoutBtn.TabIndex = 0;
            this.logoutBtn.Text = "Logout";
            this.logoutBtn.UseVisualStyleBackColor = true;
            this.logoutBtn.Click += new System.EventHandler(this.logoutBtn_Click);
            // 
            // exitBtn
            // 
            this.exitBtn.Location = new System.Drawing.Point(512, 82);
            this.exitBtn.Name = "exitBtn";
            this.exitBtn.Size = new System.Drawing.Size(75, 25);
            this.exitBtn.TabIndex = 1;
            this.exitBtn.Text = "Exit";
            this.exitBtn.UseVisualStyleBackColor = true;
            this.exitBtn.Click += new System.EventHandler(this.exitBtn_Click);
            // 
            // ViewSalesBtn
            // 
            this.ViewSalesBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(85)))), ((int)(((byte)(85)))));
            this.ViewSalesBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ViewSalesBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ViewSalesBtn.ForeColor = System.Drawing.Color.White;
            this.ViewSalesBtn.Location = new System.Drawing.Point(45, 260);
            this.ViewSalesBtn.Name = "ViewSalesBtn";
            this.ViewSalesBtn.Size = new System.Drawing.Size(150, 35);
            this.ViewSalesBtn.TabIndex = 21;
            this.ViewSalesBtn.Text = "View Sales";
            this.ViewSalesBtn.UseVisualStyleBackColor = false;
            this.ViewSalesBtn.Click += new System.EventHandler(this.ViewSalesBtn_Click);
            // 
            // BillingRecordsBtn
            // 
            this.BillingRecordsBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(85)))), ((int)(((byte)(85)))));
            this.BillingRecordsBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BillingRecordsBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BillingRecordsBtn.ForeColor = System.Drawing.Color.White;
            this.BillingRecordsBtn.Location = new System.Drawing.Point(45, 180);
            this.BillingRecordsBtn.Name = "BillingRecordsBtn";
            this.BillingRecordsBtn.Size = new System.Drawing.Size(150, 35);
            this.BillingRecordsBtn.TabIndex = 20;
            this.BillingRecordsBtn.Text = "Billing Records";
            this.BillingRecordsBtn.UseVisualStyleBackColor = false;
            this.BillingRecordsBtn.Click += new System.EventHandler(this.BillingRecordsBtn_Click);
            // 
            // CallRecordsBtn
            // 
            this.CallRecordsBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(85)))), ((int)(((byte)(85)))));
            this.CallRecordsBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CallRecordsBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CallRecordsBtn.ForeColor = System.Drawing.Color.White;
            this.CallRecordsBtn.Location = new System.Drawing.Point(45, 140);
            this.CallRecordsBtn.Name = "CallRecordsBtn";
            this.CallRecordsBtn.Size = new System.Drawing.Size(150, 35);
            this.CallRecordsBtn.TabIndex = 19;
            this.CallRecordsBtn.Text = "Call Records";
            this.CallRecordsBtn.UseVisualStyleBackColor = false;
            this.CallRecordsBtn.Click += new System.EventHandler(this.CallRecordsBtn_Click);
            // 
            // CustomerInformationBtn
            // 
            this.CustomerInformationBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(85)))), ((int)(((byte)(85)))));
            this.CustomerInformationBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CustomerInformationBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CustomerInformationBtn.ForeColor = System.Drawing.Color.White;
            this.CustomerInformationBtn.Location = new System.Drawing.Point(45, 100);
            this.CustomerInformationBtn.Name = "CustomerInformationBtn";
            this.CustomerInformationBtn.Size = new System.Drawing.Size(150, 35);
            this.CustomerInformationBtn.TabIndex = 18;
            this.CustomerInformationBtn.Text = "Customer Information";
            this.CustomerInformationBtn.UseVisualStyleBackColor = false;
            this.CustomerInformationBtn.Click += new System.EventHandler(this.CustomerInformationBtn_Click);
            // 
            // ComplaintBtn
            // 
            this.ComplaintBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(85)))), ((int)(((byte)(85)))));
            this.ComplaintBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ComplaintBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ComplaintBtn.ForeColor = System.Drawing.Color.White;
            this.ComplaintBtn.Location = new System.Drawing.Point(45, 220);
            this.ComplaintBtn.Name = "ComplaintBtn";
            this.ComplaintBtn.Size = new System.Drawing.Size(150, 35);
            this.ComplaintBtn.TabIndex = 17;
            this.ComplaintBtn.Text = "Compaign";
            this.ComplaintBtn.UseVisualStyleBackColor = false;
            this.ComplaintBtn.Click += new System.EventHandler(this.ComplaintBtn_Click);
            // 
            // ViewMemoBtn
            // 
            this.ViewMemoBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(85)))), ((int)(((byte)(85)))));
            this.ViewMemoBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ViewMemoBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ViewMemoBtn.ForeColor = System.Drawing.Color.White;
            this.ViewMemoBtn.Location = new System.Drawing.Point(45, 60);
            this.ViewMemoBtn.Name = "ViewMemoBtn";
            this.ViewMemoBtn.Size = new System.Drawing.Size(150, 35);
            this.ViewMemoBtn.TabIndex = 16;
            this.ViewMemoBtn.Text = "Memo";
            this.ViewMemoBtn.UseVisualStyleBackColor = false;
            this.ViewMemoBtn.Click += new System.EventHandler(this.ViewMemoBtn_Click);
            // 
            // SellNumberBtn
            // 
            this.SellNumberBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(85)))), ((int)(((byte)(85)))));
            this.SellNumberBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SellNumberBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SellNumberBtn.ForeColor = System.Drawing.Color.White;
            this.SellNumberBtn.Location = new System.Drawing.Point(45, 300);
            this.SellNumberBtn.Name = "SellNumberBtn";
            this.SellNumberBtn.Size = new System.Drawing.Size(150, 35);
            this.SellNumberBtn.TabIndex = 22;
            this.SellNumberBtn.Text = "Sell Number";
            this.SellNumberBtn.UseVisualStyleBackColor = false;
            this.SellNumberBtn.Click += new System.EventHandler(this.SellNumberBtn_Click);
            // 
            // managedPackagesBtn
            // 
            this.managedPackagesBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(85)))), ((int)(((byte)(85)))));
            this.managedPackagesBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.managedPackagesBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.managedPackagesBtn.ForeColor = System.Drawing.Color.White;
            this.managedPackagesBtn.Location = new System.Drawing.Point(45, 340);
            this.managedPackagesBtn.Name = "managedPackagesBtn";
            this.managedPackagesBtn.Size = new System.Drawing.Size(150, 35);
            this.managedPackagesBtn.TabIndex = 24;
            this.managedPackagesBtn.Text = "View Packages";
            this.managedPackagesBtn.UseVisualStyleBackColor = false;
            this.managedPackagesBtn.Click += new System.EventHandler(this.managedPackagesBtn_Click);
            // 
            // OfficeManagerInterface
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::CustomerManagementSystems.Properties.Resources.role;
            this.ClientSize = new System.Drawing.Size(599, 396);
            this.Controls.Add(this.managedPackagesBtn);
            this.Controls.Add(this.SellNumberBtn);
            this.Controls.Add(this.ViewSalesBtn);
            this.Controls.Add(this.BillingRecordsBtn);
            this.Controls.Add(this.CallRecordsBtn);
            this.Controls.Add(this.CustomerInformationBtn);
            this.Controls.Add(this.ComplaintBtn);
            this.Controls.Add(this.ViewMemoBtn);
            this.Controls.Add(this.exitBtn);
            this.Controls.Add(this.logoutBtn);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "OfficeManagerInterface";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Dashboard - Office Manager";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button logoutBtn;
        private System.Windows.Forms.Button exitBtn;
        private System.Windows.Forms.Button ViewSalesBtn;
        private System.Windows.Forms.Button BillingRecordsBtn;
        private System.Windows.Forms.Button CallRecordsBtn;
        private System.Windows.Forms.Button CustomerInformationBtn;
        private System.Windows.Forms.Button ComplaintBtn;
        private System.Windows.Forms.Button ViewMemoBtn;
        private System.Windows.Forms.Button SellNumberBtn;
        private System.Windows.Forms.Button managedPackagesBtn;
    }
}