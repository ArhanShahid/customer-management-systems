﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.IO;

namespace CustomerManagementSystems
{
    class FeedBack
    {
        public DataTable getFeedBack(string qurey)
        {
            QueryManager query = new QueryManager();
            return query.getTableData(qurey);
        }

        public void addFeedback(string qurey)
        {
            QueryManager query = new QueryManager();
            query.DBInsertUpdateDelete(qurey, "Feedback Added");
        }
    }
}
