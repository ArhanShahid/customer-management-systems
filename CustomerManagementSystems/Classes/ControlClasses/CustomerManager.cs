﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.IO;

namespace CustomerManagementSystems
{
    class CustomerManager
    {
         CustomerInfo c;
         CallRecords cr;
         Billing b;
         Camplaint camplaint;
         FeedBack fb;
         Packages package;
         DiscountOffers discountOffers;
              
        public DataTable getCustomerInfo()
        {

            c = new CustomerInfo();
            string[] lines = File.ReadAllLines("info.txt");
            string qurey = "SELECT * FROM single_customer_info WHERE [customer ID]=" + lines[0] + "";

            DataTable dt = c.selectInfo(qurey);
            return dt;
        }

        public void updateCustomerInfo(string cid,string editNameTB,string editEmailTB,string editUserNameTB,string editPasswordTB)
        {
            c = new CustomerInfo();
            c.updateCustomerInfo(cid,editNameTB, editEmailTB, editUserNameTB, editPasswordTB);
        }

        public DataTable getCustomerCallRecords()
        {
            cr = new CallRecords();
            string[] lines = File.ReadAllLines("info.txt");
            string query = "select * FROM call_Record_view WHERE [Call From Name]='" + lines[1] + "'";
            DataTable dt = cr.getCallRecords(query);
            return dt;
        }
        public DataTable getBillingRecords()
        {
            b = new Billing();
            string[] lines = File.ReadAllLines("info.txt");
            string query = "SELECT * FROM Billing_Record WHERE [User Name]='" + lines[1] + "'";
            return b.getBillingRecords(query);
        }
        public DataTable getCamplaint()
        {
            camplaint = new Camplaint();
            string[] lines = File.ReadAllLines("info.txt");
            string query = "Select * FROM Complaint_view WHERE [Customer User Name]='" + lines[1] + "'";
            return camplaint.getCamplaint(query);
        }
        public DataTable getFeedback()
        {
            fb = new FeedBack();
            string[] lines = File.ReadAllLines("info.txt");
            string query = "SELECT * FROM Feedback_view	WHERE [Customer User Name]='" + lines[1] + "'";
            return fb.getFeedBack(query);
        }

        public void addFeedback(string title,string desc)
        {
            fb = new FeedBack();
            string[] lines = File.ReadAllLines("info.txt");
            string query = "INSERT INTO customer_feedback(cfb_title,cfb_desc,cfb_cid) VALUES('" + title + "','"+desc+"',"+lines[0]+")";
            fb.addFeedback(query);
        }
        public void addCamplaint(string title,string desc,int depNo)
        {
            camplaint = new Camplaint();
            string[] lines = File.ReadAllLines("info.txt");
            string query = "INSERT INTO customer_complaint(cc_title,cc_desc,cc_department,cc_cid) VALUES('" + title + "','" + desc + "'," + depNo + ","+lines[0]+")";
            camplaint.addCamplaint(query);
        }

        public DataTable getPackages()
        {
            package = new Packages();
            string[] lines = File.ReadAllLines("info.txt");
            string query = "SELECT * FROM customer_package_view WHERE [User Name]='" + lines[1] + "'";
            return package.getPackages(query);
        }
        public DataTable getDiscountOffers()
        {
            discountOffers = new DiscountOffers();
            string query = "SELECT * FROM discount_offer_view";
            return discountOffers.getDiscountOffers(query);
        }
    }
}
