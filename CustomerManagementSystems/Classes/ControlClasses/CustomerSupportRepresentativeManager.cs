﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.IO;

namespace CustomerManagementSystems
{
    class CustomerSupportRepresentativeManager
    {
        CallRecords cr;
        Memo memo;
        CustomerInfo ci;
        Billing b;
        Camplaint camplaint;
        FeedBack fb;

        public DataTable getCustomerCallRecords()
        {
            cr = new CallRecords();
            string query = "select * FROM call_Record_view";
            DataTable dt = cr.getCallRecords(query);
            return dt;
        }
        public DataTable getMemo()
        {
            memo = new Memo();
            string[] lines = File.ReadAllLines("info.txt");
            string query = "SELECT * FROM Memo_View	WHERE [USER NAME]='" + lines[1] + "'";
            DataTable dt = memo.getMemo(query);
            return dt;
        }
        public DataTable getAllCustomerInfo()
        {
            ci = new CustomerInfo();
            string query = "SELECT * FROM ALL_CustomerInfo_View";
            DataTable dt = ci.selectInfo(query);
            return dt;
        }
        public DataTable getBillingRecords()
        {
            b = new Billing();
            string query = "SELECT * FROM Billing_Record";
            return b.getBillingRecords(query);
        }
        public DataTable getCamplaint()
        {
            camplaint = new Camplaint();
            string query = "Select * FROM Complaint_view";
            return camplaint.getCamplaint(query);
        }
        public DataTable getFeedback()
        {
            fb = new FeedBack();
            string[] lines = File.ReadAllLines("info.txt");
            string query = "SELECT * FROM Feedback_view";
            return fb.getFeedBack(query);
        }
    }
}
