﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.IO;

namespace CustomerManagementSystems
{
    class MarketingManager
    {
        Sales sale;
        Memo memo;
        Campaign campaign;
        public DataTable getSalesRecords()
        {
            sale = new Sales();
            string query = "SELECT * FROM Sales_view";
            DataTable dt = sale.getSalesRecords(query);
            return dt;
        }
        public DataTable getMemo()
        {
            memo = new Memo();
            string[] lines = File.ReadAllLines("info.txt");
            string query = "SELECT * FROM Memo_View	WHERE [USER NAME]='"+ lines[1] +"'";
            DataTable dt = memo.getMemo(query);
            return dt;
        }
        public DataTable getCampaign()
        {
            campaign = new Campaign();
            string query = "SELECT * FROM Marketing_view";
            DataTable dt = campaign.getCampaign(query);
            return dt;
        }

        public void addCampaign(string title,string desc,int budget,int deprtnum)
        {
            campaign = new Campaign();
            string query = "INSERT INTO marketing_campaign(mc_title,mc_desc,mc_budget,mc_department) VALUES('" + title + "','"+desc+"',"+budget+","+deprtnum+")";
            campaign.addCampaign(query);
        }
    }
}
