﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.IO;

namespace CustomerManagementSystems
{
    class QueryManager:Connection
    {
        //To Get Data Tables
        public DataTable getTableData(string qurey)
        {
            
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter();
            CustomerInfoInterface ciu = new CustomerInfoInterface();
            open_connection();
            SqlCommand cmd = con.CreateCommand();
            cmd.CommandText = qurey;
            da.SelectCommand = cmd;
            da.Fill(dt);
            cmd.Dispose();
            da.Dispose();
            close_connection();
            return dt;
 
        }
        //To Execute Insert Update Delete Queryes
        public void DBInsertUpdateDelete(string query,string success)
        {
            try
            {
                open_connection();
                SqlCommand cmd = new SqlCommand(query, con);
                cmd.ExecuteNonQuery();
                close_connection();
                SuccessInterface sc = new SuccessInterface(success);
                sc.Show();
            }
            catch (Exception ex)
            {
                ErrorInterface er = new ErrorInterface(ex.Message);
                er.Show();
            }
        }
    }
}
