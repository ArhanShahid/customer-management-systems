﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.IO;


namespace CustomerManagementSystems
{
    class OfficeManager
    {
        CallRecords cr;
        Sales sale;
        Memo memo;
        Campaign campaign;
        CustomerInfo ci;
        Billing b;
        Numbers num;
        Packages package;  

        public DataTable getCustomerCallRecords()
        {
            cr = new CallRecords();
            string query = "select * FROM call_Record_view";
            DataTable dt = cr.getCallRecords(query);
            return dt;
        }
       
        public DataTable getSalesRecords()
        {
            sale = new Sales();
            string query = "SELECT * FROM Sales_view";
            DataTable dt = sale.getSalesRecords(query);
            return dt;
        }
        public DataTable getMemo()
        {
            memo = new Memo();
            string[] lines = File.ReadAllLines("info.txt");
            string query = "SELECT * FROM Memo_View	WHERE [USER NAME]='" + lines[1] + "'";
            DataTable dt = memo.getMemo(query);
            return dt;
        }
        public DataTable getCampaign()
        {
            campaign = new Campaign();
            string query = "SELECT * FROM Marketing_view";
            DataTable dt = campaign.getCampaign(query);
            return dt;
        }

        public DataTable getAllCustomerInfo()
        {
            ci = new CustomerInfo();
            string query = "SELECT * FROM ALL_CustomerInfo_View";
            DataTable dt = ci.selectInfo(query);
            return dt;
        }
        public DataTable getBillingRecords()
        {
            b = new Billing();
            string query = "SELECT * FROM Billing_Record";
            return b.getBillingRecords(query);
        }
        public DataTable getAvailableNumber()
        {
            num = new Numbers();
            string query = "SELECT * FROM mobile_numbers WHERE mn_available=1";
            return num.getAvailableNumber(query);
        }
        public DataTable getPackages()
        {
            package = new Packages();
            string query = "SELECT * FROM all_package_info";
            return package.getPackages(query);
        }
    }
}
