﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace CustomerManagementSystems
{
    public partial class AddFeedbackInterface : Form
    {
        public AddFeedbackInterface()
        {
            InitializeComponent();
        }

        private void logoutBtn_Click(object sender, EventArgs e)
        {
            if (File.Exists("info.txt"))
            {
                File.Delete("info.txt");
            }
            this.Hide();
            LoginInterface log = new LoginInterface();
            log.Show();
        }

        private void exitBtn_Click(object sender, EventArgs e)
        {
            if (File.Exists("info.txt"))
            {
                File.Delete("info.txt");
            }
            Application.Exit();
        }

        
        private void AddFeedback_Click(object sender, EventArgs e)
        {
             CustomerManager cm = new CustomerManager();
             cm.addFeedback(fbTitleTb.Text,fbDescRTb.Text);
        }

        private void fbTitleTb_TextChanged(object sender, EventArgs e)
        {
        
        }

        private void backBTN_Click(object sender, EventArgs e)
        {
            if (File.Exists("info.txt"))
            {
                String[] userArray = File.ReadAllLines("info.txt");
                int userRole = Convert.ToInt32(userArray[2]);
                switch (userRole)
                {
                    case 1:
                        CustomerInterface cdb = new CustomerInterface();
                        this.Hide();
                        cdb.Show();
                        break;

                    case 2:
                        OfficeManagerInterface omd = new OfficeManagerInterface();
                        this.Hide();
                        omd.Show();
                        break;

                    case 3:
                        MarketingManagerInterface mmd = new MarketingManagerInterface();
                        this.Hide();
                        mmd.Show();
                        break;

                    case 4:
                        CustomerSupportRepresentativeInterface csrd = new CustomerSupportRepresentativeInterface();
                        this.Hide();
                        csrd.Show();
                        break;
                    case 5:
                        CEOInterface ceod = new CEOInterface();
                        this.Hide();
                        ceod.Show();
                        break;

                    default:
                        ErrorInterface er = new ErrorInterface("Oops !!");
                        er.Show();
                        break;
                }

            }
        }
    }
}
