﻿namespace CustomerManagementSystems
{
    partial class AddCamplaintInterface
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddCamplaintInterface));
            this.exitBtn = new System.Windows.Forms.Button();
            this.logoutBtn = new System.Windows.Forms.Button();
            this.AddCamplaint = new System.Windows.Forms.Button();
            this.CamplaintRTb = new System.Windows.Forms.RichTextBox();
            this.CampTitleTb = new System.Windows.Forms.TextBox();
            this.lableDescription = new System.Windows.Forms.Label();
            this.CamplaintTitle = new System.Windows.Forms.Label();
            this.DepartmentNo = new System.Windows.Forms.Label();
            this.CampDepNoTB = new System.Windows.Forms.TextBox();
            this.backBTN = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // exitBtn
            // 
            this.exitBtn.Location = new System.Drawing.Point(512, 114);
            this.exitBtn.Name = "exitBtn";
            this.exitBtn.Size = new System.Drawing.Size(75, 25);
            this.exitBtn.TabIndex = 5;
            this.exitBtn.Text = "Exit";
            this.exitBtn.UseVisualStyleBackColor = true;
            this.exitBtn.Click += new System.EventHandler(this.exitBtn_Click);
            // 
            // logoutBtn
            // 
            this.logoutBtn.Location = new System.Drawing.Point(512, 87);
            this.logoutBtn.Name = "logoutBtn";
            this.logoutBtn.Size = new System.Drawing.Size(75, 25);
            this.logoutBtn.TabIndex = 4;
            this.logoutBtn.Text = "Logout";
            this.logoutBtn.UseVisualStyleBackColor = true;
            this.logoutBtn.Click += new System.EventHandler(this.logoutBtn_Click);
            // 
            // AddCamplaint
            // 
            this.AddCamplaint.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(85)))), ((int)(((byte)(85)))));
            this.AddCamplaint.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.AddCamplaint.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AddCamplaint.ForeColor = System.Drawing.Color.White;
            this.AddCamplaint.Location = new System.Drawing.Point(113, 336);
            this.AddCamplaint.Name = "AddCamplaint";
            this.AddCamplaint.Size = new System.Drawing.Size(363, 35);
            this.AddCamplaint.TabIndex = 16;
            this.AddCamplaint.Text = "Add Camplaint";
            this.AddCamplaint.UseVisualStyleBackColor = false;
            this.AddCamplaint.Click += new System.EventHandler(this.AddCamplaint_Click);
            // 
            // CamplaintRTb
            // 
            this.CamplaintRTb.Location = new System.Drawing.Point(113, 123);
            this.CamplaintRTb.Name = "CamplaintRTb";
            this.CamplaintRTb.Size = new System.Drawing.Size(363, 153);
            this.CamplaintRTb.TabIndex = 15;
            this.CamplaintRTb.Text = "";
            this.CamplaintRTb.TextChanged += new System.EventHandler(this.CamplaintRTb_TextChanged);
            // 
            // CampTitleTb
            // 
            this.CampTitleTb.Location = new System.Drawing.Point(113, 70);
            this.CampTitleTb.Name = "CampTitleTb";
            this.CampTitleTb.Size = new System.Drawing.Size(363, 20);
            this.CampTitleTb.TabIndex = 14;
            this.CampTitleTb.TextChanged += new System.EventHandler(this.CampTitleTb_TextChanged);
            // 
            // lableDescription
            // 
            this.lableDescription.AutoSize = true;
            this.lableDescription.BackColor = System.Drawing.Color.White;
            this.lableDescription.Location = new System.Drawing.Point(41, 123);
            this.lableDescription.Name = "lableDescription";
            this.lableDescription.Size = new System.Drawing.Size(66, 13);
            this.lableDescription.TabIndex = 13;
            this.lableDescription.Text = "Description :";
            // 
            // CamplaintTitle
            // 
            this.CamplaintTitle.AutoSize = true;
            this.CamplaintTitle.BackColor = System.Drawing.Color.White;
            this.CamplaintTitle.Location = new System.Drawing.Point(71, 75);
            this.CamplaintTitle.Name = "CamplaintTitle";
            this.CamplaintTitle.Size = new System.Drawing.Size(36, 13);
            this.CamplaintTitle.TabIndex = 12;
            this.CamplaintTitle.Text = "Title : ";
            // 
            // DepartmentNo
            // 
            this.DepartmentNo.AutoSize = true;
            this.DepartmentNo.BackColor = System.Drawing.Color.White;
            this.DepartmentNo.Location = new System.Drawing.Point(22, 301);
            this.DepartmentNo.Name = "DepartmentNo";
            this.DepartmentNo.Size = new System.Drawing.Size(85, 13);
            this.DepartmentNo.TabIndex = 17;
            this.DepartmentNo.Text = "Department No :";
            // 
            // CampDepNoTB
            // 
            this.CampDepNoTB.Location = new System.Drawing.Point(113, 298);
            this.CampDepNoTB.Name = "CampDepNoTB";
            this.CampDepNoTB.Size = new System.Drawing.Size(363, 20);
            this.CampDepNoTB.TabIndex = 18;
            this.CampDepNoTB.TextChanged += new System.EventHandler(this.CampDepNoTB_TextChanged);
            // 
            // backBTN
            // 
            this.backBTN.Location = new System.Drawing.Point(512, 60);
            this.backBTN.Name = "backBTN";
            this.backBTN.Size = new System.Drawing.Size(75, 25);
            this.backBTN.TabIndex = 19;
            this.backBTN.Text = "Back";
            this.backBTN.UseVisualStyleBackColor = true;
            this.backBTN.Click += new System.EventHandler(this.backBTN_Click);
            // 
            // AddCamplaintInterface
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::CustomerManagementSystems.Properties.Resources.role;
            this.ClientSize = new System.Drawing.Size(599, 396);
            this.Controls.Add(this.backBTN);
            this.Controls.Add(this.CampDepNoTB);
            this.Controls.Add(this.DepartmentNo);
            this.Controls.Add(this.AddCamplaint);
            this.Controls.Add(this.CamplaintRTb);
            this.Controls.Add(this.CampTitleTb);
            this.Controls.Add(this.lableDescription);
            this.Controls.Add(this.CamplaintTitle);
            this.Controls.Add(this.exitBtn);
            this.Controls.Add(this.logoutBtn);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "AddCamplaintInterface";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AddCamplaintInterface";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button exitBtn;
        private System.Windows.Forms.Button logoutBtn;
        private System.Windows.Forms.Button AddCamplaint;
        public System.Windows.Forms.RichTextBox CamplaintRTb;
        public System.Windows.Forms.TextBox CampTitleTb;
        private System.Windows.Forms.Label lableDescription;
        private System.Windows.Forms.Label CamplaintTitle;
        private System.Windows.Forms.Label DepartmentNo;
        private System.Windows.Forms.TextBox CampDepNoTB;
        private System.Windows.Forms.Button backBTN;
    }
}