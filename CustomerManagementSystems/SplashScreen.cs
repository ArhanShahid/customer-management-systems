﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CustomerManagementSystems
{
    public partial class SplashScreen : Form
    {
        int counter = 0, buffer = 0;
        public SplashScreen()
        {
            InitializeComponent();
            Opacity = 0;
            timer1.Start();
        }
       

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (Opacity == 1)
            {
                timer2.Start();
                timer1.Stop();
            }
            else
            {
                counter++;
                Opacity = counter * 0.02;
            }
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            if (buffer == 3)
            {

                timer3.Start();
                timer2.Stop();
            }
            else
            {
                buffer++;
            }
        }

        private void timer3_Tick(object sender, EventArgs e)
        {
            if (Opacity == 0)
            {
                LoginInterface loginForm = new LoginInterface();
                loginForm.Show();
                Hide();
                timer3.Stop();
            }
            else
            {
                counter--;
                Opacity = counter * 0.02;
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }
    }
}
