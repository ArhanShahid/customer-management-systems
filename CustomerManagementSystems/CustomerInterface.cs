﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace CustomerManagementSystems
{
    public partial class CustomerInterface : Form
    {
        public CustomerInterface()
        {
            InitializeComponent();
        }

        private void logoutBtn_Click(object sender, EventArgs e)
        {
            if (File.Exists("info.txt"))
            {
                File.Delete("info.txt");
            }
            this.Hide();
            LoginInterface log = new LoginInterface();
            log.Show();

        }

        private void exitBtn_Click(object sender, EventArgs e)
        {
            if (File.Exists("info.txt"))
            {
                File.Delete("info.txt");
            }
            Application.Exit();
        }
        
        private void CustomerInformationBtn_Click(object sender, EventArgs e)
        {
            CustomerManager cm = new CustomerManager();
            CustomerInfoInterface ciu = new CustomerInfoInterface();
            ciu.customerinfo.DataSource = cm.getCustomerInfo();
            this.Hide();
            ciu.Show();
        }

        private void CallRecordsBtn_Click(object sender, EventArgs e)
        {
            CustomerManager cm = new CustomerManager();
            CallRecordsInterface cri = new CallRecordsInterface();
            cri.CustomerCallRDataGridView.DataSource = cm.getCustomerCallRecords();
            this.Close();
            cri.Show();
        }

        private void BillingRecordsBtn_Click(object sender, EventArgs e)
        {
            CustomerManager cm = new CustomerManager();
            BillingInterface bi = new BillingInterface();
            bi.BillingDataGridView.DataSource = cm.getBillingRecords();
            this.Hide();
            bi.Show();
        }

        private void PackagesInformationBtn_Click(object sender, EventArgs e)
        {
            CustomerManager cm = new CustomerManager();
            CustomerPackageInterface cpi = new CustomerPackageInterface();
            cpi.PackageDataGridView.DataSource = cm.getPackages();
            this.Hide();
            cpi.Show();
        }

        private void DiscountOffersBtn_Click(object sender, EventArgs e)
        {
            CustomerManager cm = new CustomerManager();
            DiscountOffersInterface doi = new DiscountOffersInterface();
            doi.DiscountOfferDataGridView.DataSource = cm.getDiscountOffers();
            this.Hide();
            doi.Show();

        }

        private void ViewComplaintBtn_Click(object sender, EventArgs e)
        {
            CustomerManager cm = new CustomerManager();
            ComplaintInterface ci = new ComplaintInterface();
            ci.ComplaintDataView.DataSource = cm.getCamplaint();
            this.Hide();
            ci.Show();
        }

        private void viewfeedbackBtn_Click(object sender, EventArgs e)
        {
            

        }

        private void viewfeedbackBtn_Click_1(object sender, EventArgs e)
        {
            CustomerManager cm = new CustomerManager();
            FeedBackInterface fbi = new FeedBackInterface();
            fbi.FeedBackDataView.DataSource = cm.getFeedback();
            this.Hide();
            fbi.Show();
        }

        private void AddFeedback_Click(object sender, EventArgs e)
        {
            AddFeedbackInterface afi = new AddFeedbackInterface();
            this.Hide();
            afi.Show();
        }

        private void AddComplaint_Click(object sender, EventArgs e)
        {
            AddCamplaintInterface aci = new AddCamplaintInterface();
            this.Hide();
            aci.Show();

        }

       

       
    }
}
