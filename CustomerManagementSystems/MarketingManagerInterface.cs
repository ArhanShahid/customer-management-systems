﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace CustomerManagementSystems
{
    public partial class MarketingManagerInterface : Form
    {
        public MarketingManagerInterface()
        {
            InitializeComponent();
        }

        private void logoutBtn_Click(object sender, EventArgs e)
        {
            if (File.Exists("info.txt"))
            {
                File.Delete("info.txt");
            }
            this.Hide();
            LoginInterface log = new LoginInterface();
            log.Show();
        }

        private void exitBtn_Click(object sender, EventArgs e)
        {
            if (File.Exists("info.txt"))
            {
                File.Delete("info.txt");
            }
            Application.Exit();
        }

        private void ViewMemoBtn_Click(object sender, EventArgs e)
        {
            MarketingManager mm = new MarketingManager();
            MemoInterface mi = new MemoInterface();
            mi.MemoDataGridView.DataSource = mm.getMemo();
            this.Close();
            mi.Show();
        }

        private void ViewCampaignBtn_Click(object sender, EventArgs e)
        {
            MarketingManager mm = new MarketingManager();
            CampaignInterface ci = new CampaignInterface();
            ci.CampaignDataGridView.DataSource = mm.getCampaign();
            this.Close();
            ci.Show();
        }

        private void AddCampaignBtn_Click(object sender, EventArgs e)
        {
            AddCampaignInterface aci = new AddCampaignInterface();
            this.Hide();
            aci.Show();
        }

        private void ViewSalesBtn_Click(object sender, EventArgs e)
        {
            MarketingManager mm = new MarketingManager();
            SalesInterface si = new SalesInterface();
            si.SalesDataGridView.DataSource = mm.getSalesRecords();
            this.Close();
            si.Show();
        }
    }
}
