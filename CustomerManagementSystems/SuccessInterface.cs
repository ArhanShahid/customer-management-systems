﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CustomerManagementSystems
{
    public partial class SuccessInterface : Form
    {
        public SuccessInterface(string message)
        {
            InitializeComponent();
            successMessage.Text = message;
        }

        private void successBtn_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void successMessage_Click(object sender, EventArgs e)
        {

        }
    }
}
