﻿namespace CustomerManagementSystems
{
    partial class DiscountOffersInterface
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DiscountOffersInterface));
            this.exitBtn = new System.Windows.Forms.Button();
            this.logoutBtn = new System.Windows.Forms.Button();
            this.DiscountOfferDataGridView = new System.Windows.Forms.DataGridView();
            this.backBTN = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.DiscountOfferDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // exitBtn
            // 
            this.exitBtn.Location = new System.Drawing.Point(512, 106);
            this.exitBtn.Name = "exitBtn";
            this.exitBtn.Size = new System.Drawing.Size(75, 25);
            this.exitBtn.TabIndex = 3;
            this.exitBtn.Text = "Exit";
            this.exitBtn.UseVisualStyleBackColor = true;
            this.exitBtn.Click += new System.EventHandler(this.exitBtn_Click);
            // 
            // logoutBtn
            // 
            this.logoutBtn.Location = new System.Drawing.Point(512, 79);
            this.logoutBtn.Name = "logoutBtn";
            this.logoutBtn.Size = new System.Drawing.Size(75, 25);
            this.logoutBtn.TabIndex = 2;
            this.logoutBtn.Text = "Logout";
            this.logoutBtn.UseVisualStyleBackColor = true;
            this.logoutBtn.Click += new System.EventHandler(this.logoutBtn_Click);
            // 
            // DiscountOfferDataGridView
            // 
            this.DiscountOfferDataGridView.BackgroundColor = System.Drawing.Color.White;
            this.DiscountOfferDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DiscountOfferDataGridView.Location = new System.Drawing.Point(22, 140);
            this.DiscountOfferDataGridView.Name = "DiscountOfferDataGridView";
            this.DiscountOfferDataGridView.Size = new System.Drawing.Size(562, 233);
            this.DiscountOfferDataGridView.TabIndex = 4;
            // 
            // backBTN
            // 
            this.backBTN.Location = new System.Drawing.Point(512, 52);
            this.backBTN.Name = "backBTN";
            this.backBTN.Size = new System.Drawing.Size(75, 25);
            this.backBTN.TabIndex = 20;
            this.backBTN.Text = "Back";
            this.backBTN.UseVisualStyleBackColor = true;
            this.backBTN.Click += new System.EventHandler(this.backBTN_Click);
            // 
            // DiscountOffersInterface
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::CustomerManagementSystems.Properties.Resources.role;
            this.ClientSize = new System.Drawing.Size(599, 396);
            this.Controls.Add(this.backBTN);
            this.Controls.Add(this.DiscountOfferDataGridView);
            this.Controls.Add(this.exitBtn);
            this.Controls.Add(this.logoutBtn);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "DiscountOffersInterface";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DiscountOffersInterface";
            ((System.ComponentModel.ISupportInitialize)(this.DiscountOfferDataGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button exitBtn;
        private System.Windows.Forms.Button logoutBtn;
        public System.Windows.Forms.DataGridView DiscountOfferDataGridView;
        private System.Windows.Forms.Button backBTN;
    }
}