﻿namespace CustomerManagementSystems
{
    partial class ErrorInterface
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ErrorInterface));
            this.errorMessage = new System.Windows.Forms.Label();
            this.errorBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // errorMessage
            // 
            this.errorMessage.AutoSize = true;
            this.errorMessage.BackColor = System.Drawing.Color.Transparent;
            this.errorMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.errorMessage.Location = new System.Drawing.Point(280, 45);
            this.errorMessage.Name = "errorMessage";
            this.errorMessage.Size = new System.Drawing.Size(105, 16);
            this.errorMessage.TabIndex = 0;
            this.errorMessage.Text = "errorMessage";
            this.errorMessage.Click += new System.EventHandler(this.errorMessage_Click);
            // 
            // errorBtn
            // 
            this.errorBtn.Location = new System.Drawing.Point(285, 140);
            this.errorBtn.Name = "errorBtn";
            this.errorBtn.Size = new System.Drawing.Size(125, 25);
            this.errorBtn.TabIndex = 1;
            this.errorBtn.Text = "OK";
            this.errorBtn.UseVisualStyleBackColor = true;
            this.errorBtn.Click += new System.EventHandler(this.errorBtn_Click);
            // 
            // error
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::CustomerManagementSystems.Properties.Resources.err;
            this.ClientSize = new System.Drawing.Size(500, 200);
            this.Controls.Add(this.errorBtn);
            this.Controls.Add(this.errorMessage);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "error";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "error";
            this.Load += new System.EventHandler(this.error_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label errorMessage;
        private System.Windows.Forms.Button errorBtn;
    }
}