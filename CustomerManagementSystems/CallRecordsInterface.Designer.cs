﻿namespace CustomerManagementSystems
{
    partial class CallRecordsInterface
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CallRecordsInterface));
            this.exitBtn = new System.Windows.Forms.Button();
            this.logoutBtn = new System.Windows.Forms.Button();
            this.CustomerCallRDataGridView = new System.Windows.Forms.DataGridView();
            this.backBTN = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.CustomerCallRDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // exitBtn
            // 
            this.exitBtn.Location = new System.Drawing.Point(797, 135);
            this.exitBtn.Name = "exitBtn";
            this.exitBtn.Size = new System.Drawing.Size(75, 25);
            this.exitBtn.TabIndex = 5;
            this.exitBtn.Text = "Exit";
            this.exitBtn.UseVisualStyleBackColor = true;
            this.exitBtn.Click += new System.EventHandler(this.exitBtn_Click);
            // 
            // logoutBtn
            // 
            this.logoutBtn.Location = new System.Drawing.Point(797, 108);
            this.logoutBtn.Name = "logoutBtn";
            this.logoutBtn.Size = new System.Drawing.Size(75, 25);
            this.logoutBtn.TabIndex = 4;
            this.logoutBtn.Text = "Logout";
            this.logoutBtn.UseVisualStyleBackColor = true;
            this.logoutBtn.Click += new System.EventHandler(this.logoutBtn_Click);
            // 
            // CustomerCallRDataGridView
            // 
            this.CustomerCallRDataGridView.AllowUserToAddRows = false;
            this.CustomerCallRDataGridView.BackgroundColor = System.Drawing.Color.White;
            this.CustomerCallRDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.CustomerCallRDataGridView.Location = new System.Drawing.Point(27, 167);
            this.CustomerCallRDataGridView.Name = "CustomerCallRDataGridView";
            this.CustomerCallRDataGridView.Size = new System.Drawing.Size(826, 358);
            this.CustomerCallRDataGridView.TabIndex = 6;
            this.CustomerCallRDataGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.CustomerCallRDataGridView_CellContentClick);
            // 
            // backBTN
            // 
            this.backBTN.Location = new System.Drawing.Point(797, 81);
            this.backBTN.Name = "backBTN";
            this.backBTN.Size = new System.Drawing.Size(75, 25);
            this.backBTN.TabIndex = 20;
            this.backBTN.Text = "Back";
            this.backBTN.UseVisualStyleBackColor = true;
            this.backBTN.Click += new System.EventHandler(this.backBTN_Click);
            // 
            // CallRecordsInterface
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::CustomerManagementSystems.Properties.Resources.table;
            this.ClientSize = new System.Drawing.Size(884, 561);
            this.Controls.Add(this.backBTN);
            this.Controls.Add(this.CustomerCallRDataGridView);
            this.Controls.Add(this.exitBtn);
            this.Controls.Add(this.logoutBtn);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "CallRecordsInterface";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CallRecordsInterface";
            ((System.ComponentModel.ISupportInitialize)(this.CustomerCallRDataGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button exitBtn;
        private System.Windows.Forms.Button logoutBtn;
        public System.Windows.Forms.DataGridView CustomerCallRDataGridView;
        private System.Windows.Forms.Button backBTN;
    }
}