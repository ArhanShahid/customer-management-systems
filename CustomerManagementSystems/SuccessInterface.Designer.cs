﻿namespace CustomerManagementSystems
{
    partial class SuccessInterface
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SuccessInterface));
            this.successMessage = new System.Windows.Forms.Label();
            this.successBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // successMessage
            // 
            this.successMessage.AutoSize = true;
            this.successMessage.BackColor = System.Drawing.Color.Transparent;
            this.successMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.successMessage.Location = new System.Drawing.Point(280, 45);
            this.successMessage.Name = "successMessage";
            this.successMessage.Size = new System.Drawing.Size(129, 16);
            this.successMessage.TabIndex = 0;
            this.successMessage.Text = "successMessage";
            this.successMessage.Click += new System.EventHandler(this.successMessage_Click);
            // 
            // successBtn
            // 
            this.successBtn.Location = new System.Drawing.Point(285, 140);
            this.successBtn.Name = "successBtn";
            this.successBtn.Size = new System.Drawing.Size(125, 25);
            this.successBtn.TabIndex = 1;
            this.successBtn.Text = "OK";
            this.successBtn.UseVisualStyleBackColor = true;
            this.successBtn.Click += new System.EventHandler(this.successBtn_Click);
            // 
            // success
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::CustomerManagementSystems.Properties.Resources.success;
            this.ClientSize = new System.Drawing.Size(500, 200);
            this.Controls.Add(this.successBtn);
            this.Controls.Add(this.successMessage);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "success";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "success";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label successMessage;
        private System.Windows.Forms.Button successBtn;
    }
}