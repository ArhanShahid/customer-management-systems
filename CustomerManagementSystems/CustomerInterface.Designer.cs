﻿namespace CustomerManagementSystems
{
    partial class CustomerInterface
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CustomerInterface));
            this.logoutBtn = new System.Windows.Forms.Button();
            this.exitBtn = new System.Windows.Forms.Button();
            this.CustomerInformationBtn = new System.Windows.Forms.Button();
            this.CallRecordsBtn = new System.Windows.Forms.Button();
            this.BillingRecordsBtn = new System.Windows.Forms.Button();
            this.PackagesInformationBtn = new System.Windows.Forms.Button();
            this.DiscountOffersBtn = new System.Windows.Forms.Button();
            this.ViewComplaintBtn = new System.Windows.Forms.Button();
            this.viewfeedbackBtn = new System.Windows.Forms.Button();
            this.AddComplaint = new System.Windows.Forms.Button();
            this.AddFeedback = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // logoutBtn
            // 
            this.logoutBtn.Location = new System.Drawing.Point(512, 55);
            this.logoutBtn.Name = "logoutBtn";
            this.logoutBtn.Size = new System.Drawing.Size(75, 25);
            this.logoutBtn.TabIndex = 0;
            this.logoutBtn.Text = "Logout";
            this.logoutBtn.UseVisualStyleBackColor = true;
            this.logoutBtn.Click += new System.EventHandler(this.logoutBtn_Click);
            // 
            // exitBtn
            // 
            this.exitBtn.Location = new System.Drawing.Point(512, 82);
            this.exitBtn.Name = "exitBtn";
            this.exitBtn.Size = new System.Drawing.Size(75, 25);
            this.exitBtn.TabIndex = 1;
            this.exitBtn.Text = "Exit";
            this.exitBtn.UseVisualStyleBackColor = true;
            this.exitBtn.Click += new System.EventHandler(this.exitBtn_Click);
            // 
            // CustomerInformationBtn
            // 
            this.CustomerInformationBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(85)))), ((int)(((byte)(85)))));
            this.CustomerInformationBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CustomerInformationBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CustomerInformationBtn.ForeColor = System.Drawing.Color.White;
            this.CustomerInformationBtn.Location = new System.Drawing.Point(45, 94);
            this.CustomerInformationBtn.Name = "CustomerInformationBtn";
            this.CustomerInformationBtn.Size = new System.Drawing.Size(150, 35);
            this.CustomerInformationBtn.TabIndex = 2;
            this.CustomerInformationBtn.Text = "Customer Information";
            this.CustomerInformationBtn.UseVisualStyleBackColor = false;
            this.CustomerInformationBtn.Click += new System.EventHandler(this.CustomerInformationBtn_Click);
            // 
            // CallRecordsBtn
            // 
            this.CallRecordsBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(85)))), ((int)(((byte)(85)))));
            this.CallRecordsBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CallRecordsBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CallRecordsBtn.ForeColor = System.Drawing.Color.White;
            this.CallRecordsBtn.Location = new System.Drawing.Point(45, 134);
            this.CallRecordsBtn.Name = "CallRecordsBtn";
            this.CallRecordsBtn.Size = new System.Drawing.Size(150, 35);
            this.CallRecordsBtn.TabIndex = 3;
            this.CallRecordsBtn.Text = "Call Records";
            this.CallRecordsBtn.UseVisualStyleBackColor = false;
            this.CallRecordsBtn.Click += new System.EventHandler(this.CallRecordsBtn_Click);
            // 
            // BillingRecordsBtn
            // 
            this.BillingRecordsBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(85)))), ((int)(((byte)(85)))));
            this.BillingRecordsBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BillingRecordsBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BillingRecordsBtn.ForeColor = System.Drawing.Color.White;
            this.BillingRecordsBtn.Location = new System.Drawing.Point(45, 174);
            this.BillingRecordsBtn.Name = "BillingRecordsBtn";
            this.BillingRecordsBtn.Size = new System.Drawing.Size(150, 35);
            this.BillingRecordsBtn.TabIndex = 4;
            this.BillingRecordsBtn.Text = "Billing Records";
            this.BillingRecordsBtn.UseVisualStyleBackColor = false;
            this.BillingRecordsBtn.Click += new System.EventHandler(this.BillingRecordsBtn_Click);
            // 
            // PackagesInformationBtn
            // 
            this.PackagesInformationBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(85)))), ((int)(((byte)(85)))));
            this.PackagesInformationBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.PackagesInformationBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PackagesInformationBtn.ForeColor = System.Drawing.Color.White;
            this.PackagesInformationBtn.Location = new System.Drawing.Point(45, 214);
            this.PackagesInformationBtn.Name = "PackagesInformationBtn";
            this.PackagesInformationBtn.Size = new System.Drawing.Size(150, 35);
            this.PackagesInformationBtn.TabIndex = 5;
            this.PackagesInformationBtn.Text = "Packages Information";
            this.PackagesInformationBtn.UseVisualStyleBackColor = false;
            this.PackagesInformationBtn.Click += new System.EventHandler(this.PackagesInformationBtn_Click);
            // 
            // DiscountOffersBtn
            // 
            this.DiscountOffersBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(85)))), ((int)(((byte)(85)))));
            this.DiscountOffersBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.DiscountOffersBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DiscountOffersBtn.ForeColor = System.Drawing.Color.White;
            this.DiscountOffersBtn.Location = new System.Drawing.Point(45, 254);
            this.DiscountOffersBtn.Name = "DiscountOffersBtn";
            this.DiscountOffersBtn.Size = new System.Drawing.Size(150, 35);
            this.DiscountOffersBtn.TabIndex = 6;
            this.DiscountOffersBtn.Text = "Discount Offers";
            this.DiscountOffersBtn.UseVisualStyleBackColor = false;
            this.DiscountOffersBtn.Click += new System.EventHandler(this.DiscountOffersBtn_Click);
            // 
            // ViewComplaintBtn
            // 
            this.ViewComplaintBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(85)))), ((int)(((byte)(85)))));
            this.ViewComplaintBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ViewComplaintBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ViewComplaintBtn.ForeColor = System.Drawing.Color.White;
            this.ViewComplaintBtn.Location = new System.Drawing.Point(201, 94);
            this.ViewComplaintBtn.Name = "ViewComplaintBtn";
            this.ViewComplaintBtn.Size = new System.Drawing.Size(150, 35);
            this.ViewComplaintBtn.TabIndex = 7;
            this.ViewComplaintBtn.Text = "View Complaint";
            this.ViewComplaintBtn.UseVisualStyleBackColor = false;
            this.ViewComplaintBtn.Click += new System.EventHandler(this.ViewComplaintBtn_Click);
            // 
            // viewfeedbackBtn
            // 
            this.viewfeedbackBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(85)))), ((int)(((byte)(85)))));
            this.viewfeedbackBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.viewfeedbackBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.viewfeedbackBtn.ForeColor = System.Drawing.Color.White;
            this.viewfeedbackBtn.Location = new System.Drawing.Point(201, 175);
            this.viewfeedbackBtn.Name = "viewfeedbackBtn";
            this.viewfeedbackBtn.Size = new System.Drawing.Size(150, 35);
            this.viewfeedbackBtn.TabIndex = 8;
            this.viewfeedbackBtn.Text = "View Feedback";
            this.viewfeedbackBtn.UseVisualStyleBackColor = false;
            this.viewfeedbackBtn.Click += new System.EventHandler(this.viewfeedbackBtn_Click_1);
            // 
            // AddComplaint
            // 
            this.AddComplaint.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(85)))), ((int)(((byte)(85)))));
            this.AddComplaint.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.AddComplaint.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AddComplaint.ForeColor = System.Drawing.Color.White;
            this.AddComplaint.Location = new System.Drawing.Point(201, 135);
            this.AddComplaint.Name = "AddComplaint";
            this.AddComplaint.Size = new System.Drawing.Size(150, 35);
            this.AddComplaint.TabIndex = 9;
            this.AddComplaint.Text = "Add Complaint";
            this.AddComplaint.UseVisualStyleBackColor = false;
            this.AddComplaint.Click += new System.EventHandler(this.AddComplaint_Click);
            // 
            // AddFeedback
            // 
            this.AddFeedback.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(85)))), ((int)(((byte)(85)))));
            this.AddFeedback.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.AddFeedback.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AddFeedback.ForeColor = System.Drawing.Color.White;
            this.AddFeedback.Location = new System.Drawing.Point(201, 214);
            this.AddFeedback.Name = "AddFeedback";
            this.AddFeedback.Size = new System.Drawing.Size(150, 35);
            this.AddFeedback.TabIndex = 10;
            this.AddFeedback.Text = "Add Feedback";
            this.AddFeedback.UseVisualStyleBackColor = false;
            this.AddFeedback.Click += new System.EventHandler(this.AddFeedback_Click);
            // 
            // CustomerInterface
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::CustomerManagementSystems.Properties.Resources.role;
            this.ClientSize = new System.Drawing.Size(599, 396);
            this.Controls.Add(this.AddFeedback);
            this.Controls.Add(this.AddComplaint);
            this.Controls.Add(this.viewfeedbackBtn);
            this.Controls.Add(this.ViewComplaintBtn);
            this.Controls.Add(this.DiscountOffersBtn);
            this.Controls.Add(this.PackagesInformationBtn);
            this.Controls.Add(this.BillingRecordsBtn);
            this.Controls.Add(this.CallRecordsBtn);
            this.Controls.Add(this.CustomerInformationBtn);
            this.Controls.Add(this.exitBtn);
            this.Controls.Add(this.logoutBtn);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "CustomerInterface";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Dashboard - Customer";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button logoutBtn;
        private System.Windows.Forms.Button exitBtn;
        private System.Windows.Forms.Button CustomerInformationBtn;
        private System.Windows.Forms.Button CallRecordsBtn;
        private System.Windows.Forms.Button BillingRecordsBtn;
        private System.Windows.Forms.Button PackagesInformationBtn;
        private System.Windows.Forms.Button DiscountOffersBtn;
        private System.Windows.Forms.Button ViewComplaintBtn;
        private System.Windows.Forms.Button viewfeedbackBtn;
        private System.Windows.Forms.Button AddComplaint;
        private System.Windows.Forms.Button AddFeedback;
    }
}