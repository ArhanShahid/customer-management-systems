﻿namespace CustomerManagementSystems
{
    partial class CustomerInfoInterface
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CustomerInfoInterface));
            this.customerinfo = new System.Windows.Forms.DataGridView();
            this.exitBtn = new System.Windows.Forms.Button();
            this.logoutBtn = new System.Windows.Forms.Button();
            this.editNameTB = new System.Windows.Forms.TextBox();
            this.UpdateCustomerInformationBtn = new System.Windows.Forms.Button();
            this.updatePanel = new System.Windows.Forms.Panel();
            this.updateBTN = new System.Windows.Forms.Button();
            this.editPasswordTB = new System.Windows.Forms.TextBox();
            this.editUserNameTB = new System.Windows.Forms.TextBox();
            this.PasswordLable = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.EmailLable = new System.Windows.Forms.Label();
            this.editEmailTB = new System.Windows.Forms.TextBox();
            this.NameLable = new System.Windows.Forms.Label();
            this.backBTN = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.customerinfo)).BeginInit();
            this.updatePanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // customerinfo
            // 
            this.customerinfo.AllowUserToAddRows = false;
            this.customerinfo.BackgroundColor = System.Drawing.Color.White;
            this.customerinfo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.customerinfo.Location = new System.Drawing.Point(48, 173);
            this.customerinfo.Name = "customerinfo";
            this.customerinfo.Size = new System.Drawing.Size(800, 100);
            this.customerinfo.TabIndex = 0;
            this.customerinfo.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.customerinfo_CellContentClick);
            // 
            // exitBtn
            // 
            this.exitBtn.Location = new System.Drawing.Point(797, 136);
            this.exitBtn.Name = "exitBtn";
            this.exitBtn.Size = new System.Drawing.Size(75, 25);
            this.exitBtn.TabIndex = 3;
            this.exitBtn.Text = "Exit";
            this.exitBtn.UseVisualStyleBackColor = true;
            this.exitBtn.Click += new System.EventHandler(this.exitBtn_Click);
            // 
            // logoutBtn
            // 
            this.logoutBtn.Location = new System.Drawing.Point(797, 109);
            this.logoutBtn.Name = "logoutBtn";
            this.logoutBtn.Size = new System.Drawing.Size(75, 25);
            this.logoutBtn.TabIndex = 2;
            this.logoutBtn.Text = "Logout";
            this.logoutBtn.UseVisualStyleBackColor = true;
            this.logoutBtn.Click += new System.EventHandler(this.logoutBtn_Click);
            // 
            // editNameTB
            // 
            this.editNameTB.Location = new System.Drawing.Point(89, 23);
            this.editNameTB.Name = "editNameTB";
            this.editNameTB.Size = new System.Drawing.Size(245, 20);
            this.editNameTB.TabIndex = 4;
            this.editNameTB.TextChanged += new System.EventHandler(this.editNameTB_TextChanged);
            // 
            // UpdateCustomerInformationBtn
            // 
            this.UpdateCustomerInformationBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(85)))), ((int)(((byte)(85)))));
            this.UpdateCustomerInformationBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.UpdateCustomerInformationBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UpdateCustomerInformationBtn.ForeColor = System.Drawing.Color.White;
            this.UpdateCustomerInformationBtn.Location = new System.Drawing.Point(48, 294);
            this.UpdateCustomerInformationBtn.Name = "UpdateCustomerInformationBtn";
            this.UpdateCustomerInformationBtn.Size = new System.Drawing.Size(150, 35);
            this.UpdateCustomerInformationBtn.TabIndex = 5;
            this.UpdateCustomerInformationBtn.Text = "Update Information";
            this.UpdateCustomerInformationBtn.UseVisualStyleBackColor = false;
            this.UpdateCustomerInformationBtn.Click += new System.EventHandler(this.UpdateCustomerInformationBtn_Click);
            // 
            // updatePanel
            // 
            this.updatePanel.BackColor = System.Drawing.Color.Transparent;
            this.updatePanel.Controls.Add(this.updateBTN);
            this.updatePanel.Controls.Add(this.editPasswordTB);
            this.updatePanel.Controls.Add(this.editUserNameTB);
            this.updatePanel.Controls.Add(this.PasswordLable);
            this.updatePanel.Controls.Add(this.label2);
            this.updatePanel.Controls.Add(this.EmailLable);
            this.updatePanel.Controls.Add(this.editEmailTB);
            this.updatePanel.Controls.Add(this.NameLable);
            this.updatePanel.Controls.Add(this.editNameTB);
            this.updatePanel.Location = new System.Drawing.Point(48, 338);
            this.updatePanel.Name = "updatePanel";
            this.updatePanel.Size = new System.Drawing.Size(349, 211);
            this.updatePanel.TabIndex = 7;
            this.updatePanel.Visible = false;
            this.updatePanel.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // updateBTN
            // 
            this.updateBTN.Location = new System.Drawing.Point(89, 171);
            this.updateBTN.Name = "updateBTN";
            this.updateBTN.Size = new System.Drawing.Size(245, 30);
            this.updateBTN.TabIndex = 8;
            this.updateBTN.Text = "Update Now";
            this.updateBTN.UseVisualStyleBackColor = true;
            this.updateBTN.Click += new System.EventHandler(this.updateBTN_Click);
            // 
            // editPasswordTB
            // 
            this.editPasswordTB.Location = new System.Drawing.Point(89, 131);
            this.editPasswordTB.Name = "editPasswordTB";
            this.editPasswordTB.Size = new System.Drawing.Size(245, 20);
            this.editPasswordTB.TabIndex = 13;
            // 
            // editUserNameTB
            // 
            this.editUserNameTB.Location = new System.Drawing.Point(89, 96);
            this.editUserNameTB.Name = "editUserNameTB";
            this.editUserNameTB.Size = new System.Drawing.Size(245, 20);
            this.editUserNameTB.TabIndex = 11;
            // 
            // PasswordLable
            // 
            this.PasswordLable.AutoSize = true;
            this.PasswordLable.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PasswordLable.Location = new System.Drawing.Point(9, 131);
            this.PasswordLable.Name = "PasswordLable";
            this.PasswordLable.Size = new System.Drawing.Size(74, 16);
            this.PasswordLable.TabIndex = 10;
            this.PasswordLable.Text = "Password :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(38, 98);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 16);
            this.label2.TabIndex = 9;
            this.label2.Text = "User :";
            // 
            // EmailLable
            // 
            this.EmailLable.AutoSize = true;
            this.EmailLable.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.EmailLable.Location = new System.Drawing.Point(32, 64);
            this.EmailLable.Name = "EmailLable";
            this.EmailLable.Size = new System.Drawing.Size(48, 16);
            this.EmailLable.TabIndex = 7;
            this.EmailLable.Text = "EMail :";
            // 
            // editEmailTB
            // 
            this.editEmailTB.Location = new System.Drawing.Point(89, 61);
            this.editEmailTB.Name = "editEmailTB";
            this.editEmailTB.Size = new System.Drawing.Size(245, 20);
            this.editEmailTB.TabIndex = 6;
            this.editEmailTB.TextChanged += new System.EventHandler(this.editEmailTB_TextChanged);
            // 
            // NameLable
            // 
            this.NameLable.AutoSize = true;
            this.NameLable.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NameLable.Location = new System.Drawing.Point(28, 26);
            this.NameLable.Name = "NameLable";
            this.NameLable.Size = new System.Drawing.Size(51, 16);
            this.NameLable.TabIndex = 5;
            this.NameLable.Text = "Name :";
            // 
            // backBTN
            // 
            this.backBTN.Location = new System.Drawing.Point(797, 82);
            this.backBTN.Name = "backBTN";
            this.backBTN.Size = new System.Drawing.Size(75, 25);
            this.backBTN.TabIndex = 20;
            this.backBTN.Text = "Back";
            this.backBTN.UseVisualStyleBackColor = true;
            this.backBTN.Click += new System.EventHandler(this.backBTN_Click);
            // 
            // CustomerInfoInterface
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::CustomerManagementSystems.Properties.Resources.table;
            this.ClientSize = new System.Drawing.Size(884, 561);
            this.Controls.Add(this.backBTN);
            this.Controls.Add(this.updatePanel);
            this.Controls.Add(this.UpdateCustomerInformationBtn);
            this.Controls.Add(this.exitBtn);
            this.Controls.Add(this.logoutBtn);
            this.Controls.Add(this.customerinfo);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "CustomerInfoInterface";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Customer Info Option";
            this.Load += new System.EventHandler(this.CustomerInfoUI_Load);
            ((System.ComponentModel.ISupportInitialize)(this.customerinfo)).EndInit();
            this.updatePanel.ResumeLayout(false);
            this.updatePanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.DataGridView customerinfo;
        private System.Windows.Forms.Button exitBtn;
        private System.Windows.Forms.Button logoutBtn;
        private System.Windows.Forms.TextBox editNameTB;
        private System.Windows.Forms.Button UpdateCustomerInformationBtn;
        private System.Windows.Forms.Panel updatePanel;
        private System.Windows.Forms.Label NameLable;
        private System.Windows.Forms.TextBox editUserNameTB;
        private System.Windows.Forms.Label PasswordLable;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label EmailLable;
        private System.Windows.Forms.TextBox editEmailTB;
        private System.Windows.Forms.TextBox editPasswordTB;
        private System.Windows.Forms.Button updateBTN;
        private System.Windows.Forms.Button backBTN;

    }
}