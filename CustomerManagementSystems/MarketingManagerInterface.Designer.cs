﻿namespace CustomerManagementSystems
{
    partial class MarketingManagerInterface
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MarketingManagerInterface));
            this.logoutBtn = new System.Windows.Forms.Button();
            this.exitBtn = new System.Windows.Forms.Button();
            this.ViewMemoBtn = new System.Windows.Forms.Button();
            this.ViewCampaignBtn = new System.Windows.Forms.Button();
            this.AddCampaignBtn = new System.Windows.Forms.Button();
            this.ViewSalesBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // logoutBtn
            // 
            this.logoutBtn.Location = new System.Drawing.Point(512, 55);
            this.logoutBtn.Name = "logoutBtn";
            this.logoutBtn.Size = new System.Drawing.Size(75, 25);
            this.logoutBtn.TabIndex = 0;
            this.logoutBtn.Text = "Logout";
            this.logoutBtn.UseVisualStyleBackColor = true;
            this.logoutBtn.Click += new System.EventHandler(this.logoutBtn_Click);
            // 
            // exitBtn
            // 
            this.exitBtn.Location = new System.Drawing.Point(512, 82);
            this.exitBtn.Name = "exitBtn";
            this.exitBtn.Size = new System.Drawing.Size(75, 25);
            this.exitBtn.TabIndex = 1;
            this.exitBtn.Text = "Exit";
            this.exitBtn.UseVisualStyleBackColor = true;
            this.exitBtn.Click += new System.EventHandler(this.exitBtn_Click);
            // 
            // ViewMemoBtn
            // 
            this.ViewMemoBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(85)))), ((int)(((byte)(85)))));
            this.ViewMemoBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ViewMemoBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ViewMemoBtn.ForeColor = System.Drawing.Color.White;
            this.ViewMemoBtn.Location = new System.Drawing.Point(45, 80);
            this.ViewMemoBtn.Name = "ViewMemoBtn";
            this.ViewMemoBtn.Size = new System.Drawing.Size(150, 35);
            this.ViewMemoBtn.TabIndex = 3;
            this.ViewMemoBtn.Text = "Memo";
            this.ViewMemoBtn.UseVisualStyleBackColor = false;
            this.ViewMemoBtn.Click += new System.EventHandler(this.ViewMemoBtn_Click);
            // 
            // ViewCampaignBtn
            // 
            this.ViewCampaignBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(85)))), ((int)(((byte)(85)))));
            this.ViewCampaignBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ViewCampaignBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ViewCampaignBtn.ForeColor = System.Drawing.Color.White;
            this.ViewCampaignBtn.Location = new System.Drawing.Point(45, 120);
            this.ViewCampaignBtn.Name = "ViewCampaignBtn";
            this.ViewCampaignBtn.Size = new System.Drawing.Size(150, 35);
            this.ViewCampaignBtn.TabIndex = 4;
            this.ViewCampaignBtn.Text = "View Campaign";
            this.ViewCampaignBtn.UseVisualStyleBackColor = false;
            this.ViewCampaignBtn.Click += new System.EventHandler(this.ViewCampaignBtn_Click);
            // 
            // AddCampaignBtn
            // 
            this.AddCampaignBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(85)))), ((int)(((byte)(85)))));
            this.AddCampaignBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.AddCampaignBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AddCampaignBtn.ForeColor = System.Drawing.Color.White;
            this.AddCampaignBtn.Location = new System.Drawing.Point(45, 160);
            this.AddCampaignBtn.Name = "AddCampaignBtn";
            this.AddCampaignBtn.Size = new System.Drawing.Size(150, 35);
            this.AddCampaignBtn.TabIndex = 5;
            this.AddCampaignBtn.Text = "Add Campaign";
            this.AddCampaignBtn.UseVisualStyleBackColor = false;
            this.AddCampaignBtn.Click += new System.EventHandler(this.AddCampaignBtn_Click);
            // 
            // ViewSalesBtn
            // 
            this.ViewSalesBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(85)))), ((int)(((byte)(85)))));
            this.ViewSalesBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ViewSalesBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ViewSalesBtn.ForeColor = System.Drawing.Color.White;
            this.ViewSalesBtn.Location = new System.Drawing.Point(45, 200);
            this.ViewSalesBtn.Name = "ViewSalesBtn";
            this.ViewSalesBtn.Size = new System.Drawing.Size(150, 35);
            this.ViewSalesBtn.TabIndex = 6;
            this.ViewSalesBtn.Text = "View Sales";
            this.ViewSalesBtn.UseVisualStyleBackColor = false;
            this.ViewSalesBtn.Click += new System.EventHandler(this.ViewSalesBtn_Click);
            // 
            // marketingManagerDashboard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::CustomerManagementSystems.Properties.Resources.role;
            this.ClientSize = new System.Drawing.Size(599, 396);
            this.Controls.Add(this.ViewSalesBtn);
            this.Controls.Add(this.AddCampaignBtn);
            this.Controls.Add(this.ViewCampaignBtn);
            this.Controls.Add(this.ViewMemoBtn);
            this.Controls.Add(this.exitBtn);
            this.Controls.Add(this.logoutBtn);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "marketingManagerDashboard";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Dashboard - Marketing Manager ";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button logoutBtn;
        private System.Windows.Forms.Button exitBtn;
        private System.Windows.Forms.Button ViewMemoBtn;
        private System.Windows.Forms.Button ViewCampaignBtn;
        private System.Windows.Forms.Button AddCampaignBtn;
        private System.Windows.Forms.Button ViewSalesBtn;
    }
}