﻿namespace CustomerManagementSystems
{
    partial class AddCampaignInterface
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddCampaignInterface));
            this.Exit = new System.Windows.Forms.Button();
            this.logoutBtn = new System.Windows.Forms.Button();
            this.campaignTitle = new System.Windows.Forms.Label();
            this.Description = new System.Windows.Forms.Label();
            this.budget = new System.Windows.Forms.Label();
            this.DepartmentNumber = new System.Windows.Forms.Label();
            this.campaignTitleTB = new System.Windows.Forms.TextBox();
            this.campaignDepartTB = new System.Windows.Forms.TextBox();
            this.campaignBudgetTB = new System.Windows.Forms.TextBox();
            this.campaignDescRTB = new System.Windows.Forms.RichTextBox();
            this.AddCampaignBtn = new System.Windows.Forms.Button();
            this.backBTN = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Exit
            // 
            this.Exit.Location = new System.Drawing.Point(512, 119);
            this.Exit.Name = "Exit";
            this.Exit.Size = new System.Drawing.Size(75, 25);
            this.Exit.TabIndex = 3;
            this.Exit.Text = "Exit";
            this.Exit.UseVisualStyleBackColor = true;
            this.Exit.Click += new System.EventHandler(this.Exit_Click);
            // 
            // logoutBtn
            // 
            this.logoutBtn.Location = new System.Drawing.Point(512, 92);
            this.logoutBtn.Name = "logoutBtn";
            this.logoutBtn.Size = new System.Drawing.Size(75, 25);
            this.logoutBtn.TabIndex = 2;
            this.logoutBtn.Text = "Logout";
            this.logoutBtn.UseVisualStyleBackColor = true;
            this.logoutBtn.Click += new System.EventHandler(this.logoutBtn_Click);
            // 
            // campaignTitle
            // 
            this.campaignTitle.AutoSize = true;
            this.campaignTitle.BackColor = System.Drawing.Color.White;
            this.campaignTitle.Location = new System.Drawing.Point(64, 83);
            this.campaignTitle.Name = "campaignTitle";
            this.campaignTitle.Size = new System.Drawing.Size(83, 13);
            this.campaignTitle.TabIndex = 4;
            this.campaignTitle.Text = "Campaign Title :";
            // 
            // Description
            // 
            this.Description.AutoSize = true;
            this.Description.BackColor = System.Drawing.Color.White;
            this.Description.Location = new System.Drawing.Point(81, 122);
            this.Description.Name = "Description";
            this.Description.Size = new System.Drawing.Size(66, 13);
            this.Description.TabIndex = 5;
            this.Description.Text = "Description :";
            // 
            // budget
            // 
            this.budget.AutoSize = true;
            this.budget.BackColor = System.Drawing.Color.White;
            this.budget.Location = new System.Drawing.Point(100, 235);
            this.budget.Name = "budget";
            this.budget.Size = new System.Drawing.Size(47, 13);
            this.budget.TabIndex = 6;
            this.budget.Text = "Budget :";
            // 
            // DepartmentNumber
            // 
            this.DepartmentNumber.AutoSize = true;
            this.DepartmentNumber.BackColor = System.Drawing.Color.White;
            this.DepartmentNumber.Location = new System.Drawing.Point(64, 272);
            this.DepartmentNumber.Name = "DepartmentNumber";
            this.DepartmentNumber.Size = new System.Drawing.Size(85, 13);
            this.DepartmentNumber.TabIndex = 7;
            this.DepartmentNumber.Text = "Department No :";
            // 
            // campaignTitleTB
            // 
            this.campaignTitleTB.Location = new System.Drawing.Point(166, 80);
            this.campaignTitleTB.Name = "campaignTitleTB";
            this.campaignTitleTB.Size = new System.Drawing.Size(293, 20);
            this.campaignTitleTB.TabIndex = 8;
            this.campaignTitleTB.TextChanged += new System.EventHandler(this.campaignTitleTB_TextChanged);
            // 
            // campaignDepartTB
            // 
            this.campaignDepartTB.Location = new System.Drawing.Point(166, 268);
            this.campaignDepartTB.Name = "campaignDepartTB";
            this.campaignDepartTB.Size = new System.Drawing.Size(293, 20);
            this.campaignDepartTB.TabIndex = 9;
            this.campaignDepartTB.TextChanged += new System.EventHandler(this.campaignDepartTB_TextChanged);
            // 
            // campaignBudgetTB
            // 
            this.campaignBudgetTB.Location = new System.Drawing.Point(166, 233);
            this.campaignBudgetTB.Name = "campaignBudgetTB";
            this.campaignBudgetTB.Size = new System.Drawing.Size(293, 20);
            this.campaignBudgetTB.TabIndex = 10;
            this.campaignBudgetTB.TextChanged += new System.EventHandler(this.campaignBudgetTB_TextChanged);
            // 
            // campaignDescRTB
            // 
            this.campaignDescRTB.Location = new System.Drawing.Point(166, 122);
            this.campaignDescRTB.Name = "campaignDescRTB";
            this.campaignDescRTB.Size = new System.Drawing.Size(293, 96);
            this.campaignDescRTB.TabIndex = 11;
            this.campaignDescRTB.Text = "";
            this.campaignDescRTB.TextChanged += new System.EventHandler(this.campaignDescRTB_TextChanged);
            // 
            // AddCampaignBtn
            // 
            this.AddCampaignBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(221)))), ((int)(((byte)(85)))), ((int)(((byte)(85)))));
            this.AddCampaignBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.AddCampaignBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.AddCampaignBtn.ForeColor = System.Drawing.Color.White;
            this.AddCampaignBtn.Location = new System.Drawing.Point(166, 318);
            this.AddCampaignBtn.Name = "AddCampaignBtn";
            this.AddCampaignBtn.Size = new System.Drawing.Size(293, 35);
            this.AddCampaignBtn.TabIndex = 12;
            this.AddCampaignBtn.Text = "Add Campaign";
            this.AddCampaignBtn.UseVisualStyleBackColor = false;
            this.AddCampaignBtn.Click += new System.EventHandler(this.AddCampaignBtn_Click);
            // 
            // backBTN
            // 
            this.backBTN.Location = new System.Drawing.Point(512, 65);
            this.backBTN.Name = "backBTN";
            this.backBTN.Size = new System.Drawing.Size(75, 25);
            this.backBTN.TabIndex = 13;
            this.backBTN.Text = "Back";
            this.backBTN.UseVisualStyleBackColor = true;
            this.backBTN.Click += new System.EventHandler(this.backBTN_Click);
            // 
            // AddCampaignInterface
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::CustomerManagementSystems.Properties.Resources.role;
            this.ClientSize = new System.Drawing.Size(599, 396);
            this.Controls.Add(this.backBTN);
            this.Controls.Add(this.AddCampaignBtn);
            this.Controls.Add(this.campaignDescRTB);
            this.Controls.Add(this.campaignBudgetTB);
            this.Controls.Add(this.campaignDepartTB);
            this.Controls.Add(this.campaignTitleTB);
            this.Controls.Add(this.DepartmentNumber);
            this.Controls.Add(this.budget);
            this.Controls.Add(this.Description);
            this.Controls.Add(this.campaignTitle);
            this.Controls.Add(this.Exit);
            this.Controls.Add(this.logoutBtn);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "AddCampaignInterface";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AddCampaignInterface";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Exit;
        private System.Windows.Forms.Button logoutBtn;
        private System.Windows.Forms.Label campaignTitle;
        private System.Windows.Forms.Label Description;
        private System.Windows.Forms.Label budget;
        private System.Windows.Forms.Label DepartmentNumber;
        private System.Windows.Forms.TextBox campaignTitleTB;
        private System.Windows.Forms.TextBox campaignDepartTB;
        private System.Windows.Forms.TextBox campaignBudgetTB;
        private System.Windows.Forms.RichTextBox campaignDescRTB;
        private System.Windows.Forms.Button AddCampaignBtn;
        private System.Windows.Forms.Button backBTN;
    }
}